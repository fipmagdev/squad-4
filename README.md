# A Tropa da Marmita

## Conceito
Para o trabalho final do TT 2022.2 da EJCM, o squad **A Tropa da Marmita** produziu um app de Marketplace voltado para o comércio de 
marmitas: o **DeBandeja**. 
O objetivo da aplicação é fomentar o empreededorismo seja qual seja sua classe social, postando quentinhas para que usuários comprem.
No **DeBandeja**, o usuário pode tanto escolher postar uma quentinha, ou fazer um pedido delivery da mesma!

Nesse Marketplace, você pode acessar como visitante ou logar na sua conta, a diferença entre ambos é que é necessário
possuir uma conta para possuir acesso às várias funconalidades do app.

Na HomePage do aplicativo, é possivel acessar os pratos postados por outros usuários, pesquisar um determinado prato que você queira consumir e também favoritar
aquele prato que você viu, mas quer deixar para depois.

Para começar a vender seus pratos, basta selecionar o ícone '+' e preencher o formulário, para realizar pedidos, é mais fácil ainda, basta adicionar um prato a sua bandeja,
concluir a sua compra e pagar na entrega!


## Tecnologias Utilizadas
As linguagens de programação utlizadas neste projeto são: o Javascript, para o back-end, e Typescript, para o front-end. Para a geração dos ambientes de execução, foi utilizado o React Native no front-end da aplicação e o Node.js no back-end e utilizando a ORM do sequelize para comunicação com o banco de dados. E, por fim, a tecnologia utilizada para armazenar nosso banco de dados foi o sqlite.

É importante ressaltar que os instaladores de pacotes são diferentes entre as partes da aplicação.
Enquanto no front-end, os pacotes são instalados através do **yarn**, o back-end utiliza **npm** para suas instalações. É de
extrema importância não confundir ambos para evitar conflitos de resoluções no git.

Para mais informações sobre as dependências utilizadas e suas versões para gerar o app, 
acesse as pastas do projeto [back/package.json](https://gitlab.com/fipmagdev/squad-4/-/blob/main/back/package.json) (para os pacotes do back-end)
e [front/package.json](https://gitlab.com/fipmagdev/squad-4/-/blob/main/front/package.json) (para os pacotes do front-end).

## Links Importantes
Seguem abaixo, links dos softwares utilizados para auxiliar no desenvolvimento desse projeto:

- [ ] [Trello](https://trello.com/b/qjlUHSLR/squad-4-tt-222)
- [ ] [Figjam](https://www.figma.com/file/PD5XVDz07dQnGTp6yNuozG/%5BTemplate%5D-Processo-de-UX---Squad-Dev-(Copy))
- [ ] [Figma](https://www.figma.com/file/eaDa34LMUQwVW45YUYnbb7/Projeto---Squad-4?node-id=0%3A1)

## Autores do Projeto
- Gerente: Wesley Conceição
- Tech-Lead: Filipe Magalhães
- Front-End: Luiz Brasil
- Front-End: Francisco Augusto
- Front-End: Raphael Borba
- Back-End: João Rijo
- Back-End: Pedro Medeiros

## Status do Projeto
O projeto conta com integração da funcionalidade de login e por conta do prazo final do projeto ter chegado ao fim, o projeto está encerado. Atualmente, existem algumas pendências quanto a integração de outras funcionalidades.

