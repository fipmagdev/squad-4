import React from "react";
import { ProdutoCard } from "../../components/ProdutoCard";
import { produtos } from "../../constants/produtos";
import { usuario } from "../../constants/usuario";
import { Container, VoltarCampo, SetaIcon, FavoritosTexto } from "./style";
import { useNavigation } from "@react-navigation/native";

const MeusFavoritos = ()=> {
    
    const navigation = useNavigation();

    return (
        <Container>
            <VoltarCampo onPress={() => navigation.goBack()}>
                <SetaIcon source={require('../../../assets/seta.png')}/>
            </VoltarCampo>
            <FavoritosTexto>MEUS FAVORITOS</FavoritosTexto>
            {
                produtos.map((produto) => {
                    if(usuario.meusFavoritos.includes(produto.index)) {
                        return (
                            <ProdutoCard
                            key={produto.index}
                            produtoFoto={produto.foto}
                            produtoNome={produto.nome}
                            page="MeusFavoritos"
                            produtoPreço={produto.preço}
                            vendedorNome={produto.vendedor}
                            />
                        )
                    }
                }
            )}
        </Container>
    );
}

export default MeusFavoritos;