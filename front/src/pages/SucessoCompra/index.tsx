import React from "react";
import { SucessoPopUp } from "../../components/SucessoPopUp";
import { Container } from "./style";

const SucessoCompra = ()=> {
    return (
        <Container>
            <SucessoPopUp
                mensagem="Sua compra foi efetuada com sucesso!"
                textoBotão="OK"
            />
        </Container>
    );
}

export default SucessoCompra;