import styled from 'styled-components/native';
import { style } from '../../global/global';

//Estilização do container principal
export const Container = styled.View`
background-color:${style.colors.mainWhite};
flex:1;
min-height:926px;
min-width:428px;
`
//Estilização da Box que contem o icone de voltar
export const VoltarCampo = styled.TouchableOpacity`
height:35px;
width:35px;
margin-left:20px;
margin-top:15px;
`
//Estilização do icone de voltar
export const SetaIcon = styled.Image`
height:35px;
width:35px;
`
//Estilização do Texto "MEUS Produtos"
export const ProdutoTexto = styled.Text`
height:53px;
font-family:${style.fonts.r700};
font-size:40px;
color:${style.colors.mainBlack};
flex-direction:column;
justify-content:center;
align-self:center;
margin-top:10px;
`