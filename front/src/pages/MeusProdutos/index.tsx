import React from "react";
import { ProdutoCard } from "../../components/ProdutoCard";
import { produtos } from "../../constants/produtos";
import { usuario } from "../../constants/usuario";
import { Container, VoltarCampo, SetaIcon, ProdutoTexto } from "./style";
import { useNavigation } from "@react-navigation/native";

const MeusProdutos = ()=> {

    const navigation = useNavigation();
    
    return (
        <Container>
            <VoltarCampo onPress={() => navigation.goBack()}>
                <SetaIcon source={require('../../../assets/seta.png')}/>
            </VoltarCampo>
            <ProdutoTexto>MEUS PRODUTOS</ProdutoTexto>
            {
                produtos.map((produto) => {
                    if(usuario.meusProdutos.includes(produto.index)) {
                        return (
                            <ProdutoCard
                            key={produto.index}
                            produtoFoto={produto.foto}
                            produtoNome={produto.nome}
                            page="MeusProdutos"
                            produtoPreço={produto.preço}
                            vendedorNome={produto.vendedor}
                            />
                        )
                    }
                }
            )}
        </Container>
    );
}

export default MeusProdutos;