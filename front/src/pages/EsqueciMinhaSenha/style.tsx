import styled from 'styled-components/native';
import { style } from '../../global/global';

//Estilização do container principal
export const Container = styled.View`
background-color:${style.colors.mainRed};
flex:1;
min-height:926px;
min-width:428px;
`
//Estilização da imagem de Logo
export const Logo = styled.Image`
position:absolute;
height:293px;
width:293px;
align-self:center;
`
//Estilização da Box branca que contem todas informações
export const SenhaBox = styled.View`
position:relative;
height:685px;
width:428px;
background-color:${style.colors.mainWhite};
borderTopLeftRadius:50px;
borderTopRightRadius:50px;
margin-top:241px;
`
//Estilização do Texto "ESQUECI MINHA SENHA"
export const EsqueciTexto = styled.Text`
width:389px;
height:47px;
font-family:${style.fonts.r700};
font-size:35px;
color:${style.colors.mainBlack};
align-items:center;
align-self:center;
margin-top:31px;
`
//Estilização do Formulario com input que recebe o email
export const EmailInput = styled.TextInput.attrs({
    placeholderTextColor: 'rgba(0, 0, 0, 0.8)'
    })`
    width:386px;
    height:40px;
    background-image:url("../....//assets/emailIcon.png");
    background-repeat:no-repeat;
    borderBottomWidth:2px;
    borderBottomColor:${style.colors.mainBlack};
    borderTopLeftRadius:15px;
    borderTopRightRadius:15px;
    padding-left:63px;
    font-family:${style.fonts.r700};
    font-size:25px;
    color:rgba(0, 0, 0, 0.8);
`
//Estilização do Formulario que contem o input de email
export const Formulario = styled.View`
    align-self:center;
    margin-top:48px;
    padding-left:22px;
    height:40px;
`
//Estilização do texto auxiliar para alteração de senha
export const SuporteTexto = styled.Text`
width:381px;
font-family:${style.fonts.r700};
font-size:18px;
color:${style.colors.mainBlack};
text-align:center;
align-self:center;
margin-top:44px;
`
//Estilização dao botão Enviar
export const EnviarBotão = styled.TouchableOpacity`
width:271px;
height:75px;
background:${style.colors.secondaryRed};
border-radius:15px;
flex-direction:column;
justify-content:center;
align-self:center;
margin-top:62px;
`
//Estilização do texto interno do botão
export const TextoBotão = styled.Text`
height:47px;
font-family:${style.fonts.r700};
font-size: 40px;
flex-direction:column;
justify-content:center;
align-self:center;
color:${style.colors.mainWhite};
`
//Estilização do texto para Voltar ao Login
export const VoltarTexto = styled.Text`
height:29px;
font-family:${style.fonts.r700};
font-weight:600;
font-size:25px;
flex-direction:column;
justify-content:center;
align-self:center;
color:${style.colors.mainBlack};;
margin-top:35px;
`
//Estilização da mensagem de erro
export const TextError = styled.Text`
    font-family:${style.fonts.r700};
    font-size:18px;
    color:#D65252;
    margin-top:5px;
    margin-left:44px;
`