import React from "react";
import { useForm, Controller } from 'react-hook-form';
import { Container, EmailInput, EnviarBotão, EsqueciTexto, Formulario, Logo, SenhaBox, SuporteTexto, TextError, TextoBotão, VoltarTexto } from "./style";
import { useNavigation } from "@react-navigation/native";

interface  FormData {
    email: string
}

const EsqueciMinhaSenha = ()=> {

    const { control, handleSubmit, formState:{errors} } = useForm<FormData>({mode: "onTouched"});

    const onSubmit = (data: FormData) => {
        console.log(data);
    }

    const navigation = useNavigation();

    return (
        <Container>
            <Logo source={require('../../../assets/logo.png')}/>
            <SenhaBox>
                <EsqueciTexto>ESQUECI MINHA SENHA</EsqueciTexto>
                <Formulario>
                <Controller
                    control={control}
                    name='email'
                    defaultValue=''
                    render={({field:{onBlur, onChange, value}}) => (
                        <EmailInput
                            onBlur={onBlur}
                            onChangeText={(value:any) => onChange(value)}
                            value={value}
                            placeholder='Informe seu email'
                            maxLength={256}
                        />
                        
                    )}
                    rules={{
                        required: "Email é obrigatório",
                        pattern: {
                            value: /^[A-Z0-9._-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                            message: "Formato de email inválido"
                        },
                    }}
                />
                {errors.email && <TextError>{errors.email.message}</TextError>}
                </Formulario>
                <SuporteTexto>Você receberá por email um link para cadastrar uma nova senha</SuporteTexto>
                <EnviarBotão onPress={handleSubmit(onSubmit)}>
                    <TextoBotão>Enviar</TextoBotão>
                </EnviarBotão>
                <VoltarTexto onPress={() => navigation.navigate('Login' as never)}>Voltar ao Login</VoltarTexto>
            </SenhaBox>
        </Container>
    );
}

export default EsqueciMinhaSenha;