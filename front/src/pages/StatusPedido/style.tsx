import styled from 'styled-components/native';
import { style } from '../../global/global';

//Estilização do container principal
export const Container = styled.View`
background-color:${style.colors.mainWhite};
flex:1;
min-height:926px;
min-width:428px;
`
//Estilização da Box superior que contem o campo de favorito e campo de voltar
export const SuperiorCampo = styled.View`
height:35px;
width:380px;
flex-direction:row;
justify-content:space-between;
align-self:center;
margin-top:15px;
`
//Estilização do Botão que contem o icone de favorito
export const FavoritoCampo = styled.TouchableOpacity`
height:30px;
width:30px;
`
//Estilização do icone de favorito
export const FavoritoIcon = styled.Image`
height:35px;
width:35px;
`
//Estilização do Botão que contem o icone de voltar
export const VoltarCampo = styled.TouchableOpacity`
height:35px;
width:35px;
`
//Estilização do icone de voltar
export const VoltarIcon = styled.Image`
height:35px;
width:35px;
`
//Estilização do Texto "STATUS DO PEDIDO"
export const StatusTexto = styled.Text`
height:41px;
font-family:${style.fonts.r700};
font-size:32.5px;
color:${style.colors.mainBlack};
flex-direction:column;
justify-content:center;
align-self:center;
margin-top:16px;
`
//Estilização do Texto para o código do pedido
export const CodigoTexto = styled.Text`
height:21px;
font-family:${style.fonts.r700};
font-size:15px;
color:${style.colors.mainBlack};
flex-direction:column;
justify-content:center;
align-self:center;
`
//Estilização do ícone de refeição
export const RefeiçãoIcon = styled.Image`
height:200px;
width:266.51px;
align-self:center;
margin-top:19px;
`
//Estilização do ícone de relógio principal
export const RelogioIcon = styled.Image`
height:50px;
width:50px;
`
//Estilização do ícone de entrega em andamento
export const EntregaIcon = styled.Image`
height:75px;
width:75px;
`
//Estilização do ícone de entrega concluída
export const ConcluidoIcon = styled.Image`
height:50px;
width:50px;
`
//Estilização do ícone de relógio secundário
export const RelogioMini = styled.Image`
height:20px;
width:20px;
`
//Estilização do ícone para linha que liga o ícone de Relógio principal até o de entrega em andamento
export const Linha1 = styled.Image`
position:absolute;
height:91px;
width:1.5px;
margin-left:68px;
margin-top:414px;
`
//Estilização do ícone para linha que liga o ícone entrega em andamento até o de entrega concluída
export const Linha2 = styled.Image`
position:absolute;
height:89px;
width:1.5px;
margin-left:68px;
margin-top:552.25px;
`
//Estilização da Box que contem as informações de estimativa de tempo para entrega
export const PedidoDataCampo = styled.View`
height:20px;
width:310px;
flex-direction:row;
align-items:center;
`
//Estilização das informações de estimativa de tempo para entrega
export const PedidoData = styled.Text`
font-family:${style.fonts.r700};
font-size:15px;
color:${style.colors.mainBlack};
flex-direction:column;
justify-content:center;
margin-left:5px;
`
//Estilização da Box que contem a etapa de pedido feito
export const PedidoFeitoCampo = styled.View`
height:57px;
width:262px;
flex-direction:row;
margin-left:45px;
margin-top:20px;
`
//Estilização da Box que contem as informações da etapa de pedido feito
export const PedidoFeitoInfo = styled.View`
height:57px;
width:310px;
flex-direction:column;
margin-left:15px;
`
//Estilização das informações da etapa de pedido feito
export const PedidoFeitoTexto = styled.Text`
height:35px;
font-family:${style.fonts.r700};
font-size:30px;
color${style.colors.mainBlack};
flex-direction:column;
justify-content:center;
`
//Estilização da Box que contem a etapa de pedido em andamento
export const PedidoAndamentoCampo = styled.View`
height:75px;
width:394px;
flex-direction:row;
align-items:center;
margin-left:20px;
margin-top:66px;
`

//Estilização da Box que contem as informações da etapa de pedido em andamento
export const PedidoAndamentoInfo = styled.View`
height:50px;
width:304px;
margin-left:15px;
`
//Estilização das informações da etapa de pedido em andamento
export const PedidoAndamentoTexto = styled.Text`
font-family:${style.fonts.r700};
font-size:26.25px;
color:${style.colors.mainBlack};
flex-direction:column;
justify-content:center;
`
//Estilização da Box que contem a etapa de pedido entregue
export const PedidoEntregueCampo = styled.View`
height:60px;
width:294px;
flex-direction:row;
align-items:center;
margin-left:45px;
margin-top:73px;
`
//Estilização da Box que contem as informações da etapa de pedido entregue
export const PedidoEntregueInfo = styled.View`
height:52px;
width:292px;
flex-direction:column;
margin-left:15px;
`
//Estilização das informações da etapa de pedido entregue
export const PedidoEntregueTexto = styled.Text`
height:35px;
font-family:${style.fonts.r700};
font-size:30px;
color:${style.colors.mainBlack};
flex-direction:column;
justify-content:center;
`
//Estilização do botão Confirmar
export const ConfirmarBotão = styled.TouchableOpacity`
width:333px;
height:61px;
background:${style.colors.terciaryRed};
border-radius:5px;
flex-direction:column;
justify-content:center;
align-self:center;
margin-top:75px;
`
//Estilização do texto interno do botão Confirmar
export const TextoBotão = styled.Text`
font-family:${style.fonts.r700};
font-size:20px;
color:${style.colors.mainWhite};
align-self:center;
`