import React, { useState } from "react";
import { Container, SuperiorCampo, FavoritoCampo, FavoritoIcon, VoltarCampo, VoltarIcon, StatusTexto, CodigoTexto, RefeiçãoIcon, RelogioIcon, EntregaIcon, ConcluidoIcon, RelogioMini, Linha1, Linha2, PedidoData, PedidoDataCampo, PedidoFeitoCampo, PedidoFeitoInfo, PedidoFeitoTexto, PedidoAndamentoCampo, PedidoAndamentoInfo,  PedidoAndamentoTexto, PedidoEntregueCampo,  PedidoEntregueInfo, PedidoEntregueTexto, ConfirmarBotão, TextoBotão } from "./style";
import { useNavigation } from "@react-navigation/native";

const StatusPedido = ()=> {
    const [fav, setFav]=useState<Boolean>(false);
    
    const navigation = useNavigation();
    return (
        <Container>
            <SuperiorCampo>
                <VoltarCampo onPress={() => navigation.goBack()}>
                    <VoltarIcon source={require('../../../assets/seta.png')}/>
                </VoltarCampo>
                <FavoritoCampo onPress={() => setFav(!fav)}>
                        {fav?
                            (<FavoritoIcon source={require('../../assets/coração.png')}/>)
                            :((<FavoritoIcon source={require('../../assets/coraçãoVazio.png')}/>))
                        }
                </FavoritoCampo>
            </SuperiorCampo>
            <StatusTexto>STATUS DO PEDIDO</StatusTexto>
            <CodigoTexto>BFR1904SAF2022</CodigoTexto>
            <RefeiçãoIcon source={require('../../assets/refeiçãoIcon.png')}/>
            <PedidoFeitoCampo>
                <RelogioIcon source={require('../../assets/relogioIcon.png')}/>
                <PedidoFeitoInfo>
                    <PedidoFeitoTexto>PEDIDO FEITO</PedidoFeitoTexto>
                    <PedidoDataCampo>
                        <RelogioMini source={require('../../assets/relogioMini.png')}/>
                        <PedidoData>10:00, 16 de Agosto de 2022</PedidoData>
                    </PedidoDataCampo>
                </PedidoFeitoInfo>
            </PedidoFeitoCampo>
            <Linha1 source={require('../../assets/linha1.png')}/>
            <PedidoAndamentoCampo>
                <EntregaIcon source={require('../../assets/entrega.png')}/>
                <PedidoAndamentoInfo>
                    <PedidoAndamentoTexto>PEDIDO EM ANDAMENTO</PedidoAndamentoTexto>
                    <PedidoDataCampo>
                        <RelogioMini source={require('../../assets/relogioMini.png')}/>
                        <PedidoData>10:15, 16 de Agosto de 2022</PedidoData>
                    </PedidoDataCampo>
                </PedidoAndamentoInfo>
            </PedidoAndamentoCampo>
            <Linha2 source={require('../../assets/linha2.png')}/>
            <PedidoEntregueCampo>
                <ConcluidoIcon source={require('../../assets/concluido.png')}/>
                <PedidoEntregueInfo>
                    <PedidoEntregueTexto>PEDIDO ENTREGUE</PedidoEntregueTexto>
                    <PedidoDataCampo>
                        <RelogioMini source={require('../../assets/relogioMini.png')}/>
                        <PedidoData>10:45, 16 de Agosto de 2022</PedidoData>
                    </PedidoDataCampo>
                </PedidoEntregueInfo>
            </PedidoEntregueCampo>
            <ConfirmarBotão onPress={() => navigation.navigate('SucessoCompra' as never)}>
                <TextoBotão>CONFIRMAR RECEBIMENTO</TextoBotão>
            </ConfirmarBotão>
        </Container>
    );
}

export default StatusPedido;