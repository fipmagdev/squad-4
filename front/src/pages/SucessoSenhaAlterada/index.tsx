import React from "react";
import { SucessoPopUp } from "../../components/SucessoPopUp";
import { Container } from "./style";

const SucessoSenhaAlterada = ()=> {
    return (
        <Container>
            <SucessoPopUp
                mensagem="Sua senha foi alterada com sucesso!"
                textoBotão="OK"
            />
        </Container>
    );
}

export default SucessoSenhaAlterada;