import styled from 'styled-components/native';
import { style } from '../../global/global';

//Estilização do container principal
export const Container = styled.ScrollView`
background-color:${style.colors.mainWhite};
flex:1;
min-height:926px;
min-width:428px;
`
//Estilização do footer
export const Footer = styled.ScrollView`
background-color:${style.colors.mainRed};
height:100px;
width:100%;
margin-top: 50px;
`