import React, { useState } from "react";
import { HomeCard } from "../../components/HomeCard";
import { HomeHeader } from "../../components/HomeHeader";
import { SearchBar } from "../../components/SearchBar";
import { produtos } from "../../constants/produtos";
import { usuario } from "../../constants/usuario";
import { Container, Footer } from "./style";

const Home = () => {
    const [searchValue, setSearchValue] = useState('');

    const getSearchValue = (event: any) => {
        setSearchValue(event.target.value);
        console.log(searchValue);
    }

    const lowerCaseSearch = searchValue.toLowerCase();

    const filteredProducts = produtos.filter((produto) => produto.nome.toLowerCase().includes(lowerCaseSearch))

    return (
        <Container>
            <HomeHeader />
            <SearchBar texto="Oque você procura?" searchValue={getSearchValue} />
            {
                filteredProducts.map((produto) =>
                    <HomeCard
                        key={produto.index}
                        produtoFoto={produto.foto}
                        produtoNome={produto.nome}
                        isFav={produto.isFav}
                        usuarioTipo={usuario.tipo}
                        produtoPreço={produto.preço}
                        vendedorNome={produto.vendedor}
                    />
                )
            }
            <Footer></Footer>
        </Container>
    );
}

export default Home;