import styled from 'styled-components/native';
import { style } from '../../global/global';

//Estilização da Box superior que contem o nome da marca e campo de voltar
export const SuperiorCampo = styled.View`
height:78px;
width:428px;
background-color:${style.colors.mainRed};
align-self:center;
flex-direction:row;
justify-content:space-between;
`
//Estilização do Botão que contem o icone de voltar
export const SairCampo = styled.TouchableOpacity`
height:45px;
width:45px;
margin-left:20px;
margin-top:20px;
`
//Estilização do icone de voltar
export const SairIcon = styled.Image`
height:45px;
width:45px;
`
//Estilização do texto para o nome da marca
export const NomeMarca = styled.Text`
height:47px;
font-family:${style.fonts.r700};
font-size:35px;
color:${style.colors.mainWhite};
flex-direction:column;
justify-content:center;
margin-top:18px;
margin-right:25px;
`