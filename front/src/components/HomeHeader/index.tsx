import { SuperiorCampo, SairCampo, SairIcon, NomeMarca } from "./style";  
import { useNavigation } from "@react-navigation/native";

type HomeHeaderInfo = {

}

export function HomeHeader( { } : HomeHeaderInfo  ) {

    const navigation = useNavigation();

    return ( 
        <SuperiorCampo>
            <SairCampo onPress={() => navigation.navigate('Login')}>
                <SairIcon source={require('../../../assets/sair.png')}/>
            </SairCampo>
            <NomeMarca>DeBandeja</NomeMarca>
        </SuperiorCampo>
    );
}            