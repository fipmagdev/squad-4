import { SucessoCard, Logo, SucessoTexto, Botão, TextoBotão } from "./style";
import { useNavigation } from "@react-navigation/native";

type SucessoPopUpInfo = {
    mensagem: string;
    textoBotão: string;
}

export function SucessoPopUp( { mensagem, textoBotão } : SucessoPopUpInfo ) {
    
    const navigation = useNavigation();

    return ( 
        <SucessoCard>
            <Logo source={require('../../../assets/logo.png')}/>
            <SucessoTexto>{mensagem}</SucessoTexto>
            <Botão onPress={() => navigation.goBack()}>
                    <TextoBotão>{textoBotão}</TextoBotão>    
            </Botão>
        </SucessoCard>
    );
}