import styled from 'styled-components/native';
import { style } from '../../global/global';

//Estilização da Box de PopUp
export const SucessoCard = styled.View`
width:420px;
height:304px;
background-color:#FFFFFF;
border:1px solid ${style.colors.secondaryRed};
border-radius:10px;
align-self:center;
`
//Estilização do icone da logo
export const Logo = styled.Image`
height:150px;
width:150px;
align-self:center;
margin-top:15px;
`
//Estilização do texto para informar sucesso no processo
export const SucessoTexto = styled.Text`
height:20px;
font-family:${style.fonts.r700};
font-size:17.5px;
color:#000000;
justify-content:center;
align-self:center;
margin-top:31px;
`
//Estilização do botão
export const Botão = styled.TouchableOpacity`
width:300px;
height:45px;
background:${style.colors.terciaryRed};
border-radius:10px;
flex-direction:column;
justify-content:center;
align-self:center;
margin-top:12px;
margin-bottom:31px;
`
//Estilização do texto interno do botão
export const TextoBotão = styled.Text`
height:18px;
font-family:${style.fonts.r700};
font-size:25px;
flex-direction:column;
justify-content:center;
align-self:center;
margin-bottom:13.38px;
color: #FFFFFF;
`