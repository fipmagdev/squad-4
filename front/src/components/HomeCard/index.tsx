import React, { useState } from "react";
import { ProdutoCard,  ProdutoFoto, FavoritoCampo, FavoritoIcon, ProdutoInfoCampo, ProdutoNome, ProdutoPreço, EstrelasCampo, EstrelaIcon, VendedorNome } from "./style";  
import { useNavigation } from "@react-navigation/native";

type HomeCardInfo = {
    produtoFoto: string;
    produtoNome: string;
    isFav?: boolean;
    usuarioTipo: string;
    produtoPreço: number;
    vendedorNome: string;
}

export function HomeCard( { produtoFoto, produtoNome, isFav=false, usuarioTipo, produtoPreço, vendedorNome } : HomeCardInfo ) {
    const [fav, setFav]=useState<Boolean>(isFav);
    let allowFav = false;
    
    const navigation = useNavigation();
    
    switch (usuarioTipo) {
        case 'Usuário': 
            allowFav = true;
            break;
        
        case 'Moderador': 
            allowFav = true;
            break;
    }

    return ( 
            <ProdutoCard onPress={() => navigation.navigate('Product')}>
                <ProdutoFoto source={require(`../../../assets/${produtoFoto}.png`)}/>
                {allowFav?
                    (<FavoritoCampo onPress={() => setFav(!fav)}>
                        {fav?
                            (<FavoritoIcon source={require('../../../assets/coração.png')}/>)
                            :((<FavoritoIcon source={require('../../../assets/coraçãoVazio.png')}/>))
                        }
                    </FavoritoCampo>
                    ):
                    (<FavoritoCampo></FavoritoCampo>)
                }   
                <ProdutoInfoCampo>
                <ProdutoNome>{produtoNome}</ProdutoNome>
                <ProdutoPreço>R${produtoPreço},00</ProdutoPreço>
                <EstrelasCampo>
                    <EstrelaIcon source={require('../../../assets/estrelaFull.png')}/>
                    <EstrelaIcon source={require('../../../assets/estrelaFull.png')}/>
                    <EstrelaIcon source={require('../../../assets/estrelaFull.png')}/>
                    <EstrelaIcon source={require('../../../assets/estrelaFull.png')}/>
                    <EstrelaIcon source={require('../../../assets/estrelaFull.png')}/>
                </EstrelasCampo>
                <VendedorNome>{vendedorNome}</VendedorNome>
                </ProdutoInfoCampo>
            </ProdutoCard>
            );
}