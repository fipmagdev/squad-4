import styled from 'styled-components/native';
import { style } from '../../global/global';

//Estilização da Box para produto
export const ProdutoCard = styled.TouchableOpacity`
width:392px;
height:215px;
background-color:${style.colors.mainGrey};
border:1px solid ${style.colors.secondaryRed};
border-radius:15px;
align-self:center;
flex-direction:row;
margin-top:17px;
`
//Estilização da foto do produto
export const ProdutoFoto = styled.Image`
position:absolute;
height:183px;
width:193px;
border-radius:15px;
margin-left:5px;
margin-top:15px;
`
//Estilização da Box que contem o icone de favorito
export const FavoritoCampo = styled.TouchableOpacity`
width:30px;
height:30px;
margin-left:14px;
margin-top:18px;
`
//Estilização do icone de favorito
export const FavoritoIcon = styled.Image`
height:30px;
width:30px;
`
//Estilização da Box que contem as informações do produto
export const ProdutoInfoCampo = styled.View`
height:149px;
width:198px;
flex-direction:column;
align-items:center;
margin-left:153px;
margin-top:41px;
`
//Estilização do texto para o nome do produto
export const ProdutoNome = styled.Text`
height:36px;
font-family:${style.fonts.r700};
font-size:28px;
font-style:italic;
color:${style.colors.mainBlack};
flex-direction:column;
justify-content:center;
`
//Estilização do texto para o preço do produto
export const ProdutoPreço = styled.Text`
height:33px;
font-family:${style.fonts.r700};
font-size:30px;
color:${style.colors.mainBlack};
flex-direction:column;
justify-content:center;
margin-top:10px;
`
//Estilização da Box que contem os icones das estrelas de avaliação
export const EstrelasCampo = styled.View`
height:20px;
width:92.5px;
flex-direction:row;
margin-top:13px;
`
//Estilização do icone da estrela de avaliação
export const EstrelaIcon = styled.Image`
height:18.75px;
width:18.75px;
`
//Estilização do texto para o nome do vendedor
export const VendedorNome = styled.Text`
height:21px;
align-self:center;
margin-left:5px;
font-family:${style.fonts.r400};
font-style:italic;
font-size:20px;
display:flex;
align-items:center;
color:${style.colors.secondaryGrey};
margin-top:13px;
`