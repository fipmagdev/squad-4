import styled from 'styled-components/native';
import { style } from '../../global/global';

//Estilização da Box para produto
export const ProductCard = styled.TouchableOpacity`
width:392px;
height:215px;
background-color:${style.colors.mainGrey};
border:1px solid ${style.colors.secondaryRed};
border-radius:15px;
align-self:center;
margin-top:20px;
`
//Estilização do botão Remover favorito
export const RemoverBotão = styled.TouchableOpacity`
position:absolute;
height:33px;
width:40px;
background-color:${style.colors.mainRed};
borderTopRightRadius:15px;
borderBottomLeftRadius:15px;
align-self:flex-end;
flex-direction:column;
justify-content:center;
`
//Estilização do icone de Remover favorito
export const RemoverIcon = styled.Image`
height:3px;
width:14px;
align-self:center;
`
//Estilização do botão editar produto
export const EditarBotão = styled.TouchableOpacity`
position:absolute;
height:35px;
width:42.42px;
background-color:${style.colors.mainRed};
borderTopRightRadius:15px;
borderBottomLeftRadius:15px;
align-self:flex-end;
flex-direction:column;
justify-content:center;
`
//Estilização do icone de editar produto
export const EditarIcon = styled.Image`
height:25px;
width:25px;
align-self:center;
`
//Estilização da foto do produto
export const ProdutoFoto = styled.Image`
position:absolute;
height:150px;
width:193px;
border-radius:15px;
margin-left:15px;
margin-top:15px;
`
//Estilização da Box que contem o icone de favorito
export const FavoritoCampo = styled.View`
position:relative;
height:30px;
width:30px;
margin-left:21.5px;
margin-top:18px;
`
//Estilização do icone de favorito
export const FavoritoIcon = styled.Image`
height:30px;
width:30px;
`
//Estilização do texto para o preço do produto
export const ProdutoPreço = styled.Text`
position:relative;
height:33px;
font-family:${style.fonts.r700};
font-size:35px;
color:${style.colors.mainBlack};
flex-direction:column;
justify-content:center;
margin-left:218px;
`
//Estilização da Box que contem os icones das estrelas de avaliação
export const EstrelasCampo = styled.View`
position:relative;
height:20px;
width:92.5px;
flex-direction:row;
margin-left:218px;
margin-top:19px;
`
//Estilização do icone da estrela de avaliação
export const EstrelaIcon = styled.Image`
position:relative;
height:18.75px;
width:18.75px;
`
//Estilização da Box que contem as informações do Vendedor
export const VendedorCampo = styled.View`
position:relative;
height:35px;
width:170px;
flex-direction:row;
margin-left:218px;
margin-top:10px;
`
//Estilização do icone de foto do vendedor
export const VendedorFoto = styled.Image`
height:35px;
width:35px;
border:1px solid ${style.colors.mainBlack};
border-radius:360px;
`
//Estilização do texto para o nome do vendedor
export const VendedorNome = styled.Text`
height:35px;
align-self:center;
margin-left:5px;
font-family:${style.fonts.r400};
font-style:italic;
font-size:20px;
display:flex;
align-items:center;
color:${style.colors.secondaryGrey};
`
//Estilização do texto para o nome do produto
export const ProdutoNome = styled.Text`
height:36px;
font-family:${style.fonts.r700};
font-size:30px;
font-style:italic;
color:${style.colors.mainBlack};
flex-direction:column;
justify-content:center;
margin-left:12.5px;
margin-top:7px;
`