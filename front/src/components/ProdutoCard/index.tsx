import { ProductCard, RemoverBotão, RemoverIcon, EditarBotão, EditarIcon, ProdutoFoto, FavoritoCampo, FavoritoIcon, ProdutoPreço, EstrelasCampo, EstrelaIcon, VendedorCampo, VendedorFoto, VendedorNome, ProdutoNome } from "./style";
import { useNavigation } from "@react-navigation/native";

type ProdutoCardInfo = {
    produtoFoto: string;
    produtoNome: string;
    page: string;
    produtoPreço: number;
    vendedorNome: string;
}

export function ProdutoCard( { produtoFoto, produtoNome, page, produtoPreço, vendedorNome } : ProdutoCardInfo ) {
    let fav = true;
    
    const navigation = useNavigation();

    switch (page) {
        case "MeusFavoritos":
            fav = true;
            break;
    
        case "MeusProdutos":
            fav = false;
            break;
    }

    return ( 
        <ProductCard onPress={() => navigation.navigate('Product')}>
            {fav?(
                <RemoverBotão>
                    <RemoverIcon source={require('../../../assets/linha.png')}/>    
                </RemoverBotão>
            ):
            (
                <EditarBotão>
                    <EditarIcon source={require('../../../assets/editar.png')}/>    
                </EditarBotão>
            )}
            <ProdutoFoto source={require(`../../../assets/${produtoFoto}.png`)}/>
            {fav?(
                <FavoritoCampo>
                    <FavoritoIcon source={require('../../../assets/coração.png')}/>  
                </FavoritoCampo>
            ):
            (<FavoritoCampo></FavoritoCampo>
            )}  
            <ProdutoPreço>R${produtoPreço},00</ProdutoPreço>
            <EstrelasCampo>
                <EstrelaIcon source={require('../../../assets/estrelaFull.png')}/>
                <EstrelaIcon source={require('../../../assets/estrelaFull.png')}/>
                <EstrelaIcon source={require('../../../assets/estrelaFull.png')}/>
                <EstrelaIcon source={require('../../../assets/estrelaFull.png')}/>
                <EstrelaIcon source={require('../../../assets/estrelaFull.png')}/>
            </EstrelasCampo>
            <VendedorCampo>
                <VendedorFoto/>
                <VendedorNome>{vendedorNome}</VendedorNome>
            </VendedorCampo>
            <ProdutoNome>{produtoNome}</ProdutoNome>
        </ProductCard>
    );
}