import styled from 'styled-components/native';
import { style } from '../../global/global';

//Estilização do input de pesquisa por nome de produtos
export const SearchInput = styled.TextInput`
position:absolute;
height:50px;
width:385px;
border:1px solid ${style.colors.mainRed};
border-radius:15px;
align-self:center;
margin-top:93px;
padding-left:60px;
padding-right:15px;
flex-direction:row;
align-items:center;
font-family:${style.fonts.r700};
font-size:20px;
color:${style.colors.secondaryGrey};
`
//Estilização da Box que contem os icones de filtro e pesquisa
export const FiltroCampo = styled.View`
position:relative;
height:30px;
width:30px;
align-items:center;
flex-direction:row;
margin-top:25px;
margin-bottom:10px;
margin-left:32px;
`
//Estilização do icone triangular para filtro
export const FiltroIcon = styled.Image`
height:12.5px;
width:15px;
`
//Estilização do icone de linha divisora
export const LinhaSearch = styled.Image`
height:30px;
width:1.5px;
margin-left:15px;
`
//Estilização da Box que contem o icone de lupa
export const LupaCampo = styled.TouchableOpacity`
height:25px;
width:25px;
margin-left:297px;
`
//Estilização do icone de lupa
export const LupaIcon = styled.Image`
height:25px;
width:25px;
`