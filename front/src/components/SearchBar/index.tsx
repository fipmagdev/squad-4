import { SearchInput, LinhaSearch, FiltroIcon, FiltroCampo, LupaIcon, LupaCampo } from "./style";  

type SearchBarInfo = {
    texto: string;
    searchValue: (event : any) => void;
}

export function SearchBar( { texto, searchValue } : SearchBarInfo ) {
    return ( 
        <>
            <SearchInput placeholder={texto} onChange={(event: any) => searchValue(event)}></SearchInput>
            <FiltroCampo>
                <FiltroIcon source={require('../../../assets/filtroIcon.png')}/>
                <LinhaSearch source={require('../../../assets/linhaSearch.png')}/>
                <LupaCampo>
                    <LupaIcon source={require('../../../assets/lupa.png')}/>
                </LupaCampo>
            </FiltroCampo>
        </>
    );
}