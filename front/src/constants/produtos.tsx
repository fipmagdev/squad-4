export const produtos = [
    {
        index: 1,
        foto: 'refeição',
        nome: 'Prato da Casa',
        isFav: true,
        preço: 15,
        vendedor: 'Tio João',
    }, 
    {
        index: 2,
        foto: 'refeição',
        nome: 'Prato de Fora',
        isFav: false,
        preço: 15,
        vendedor: 'Tio João',
    },
    {
        index: 3,
        foto: 'refeição',
        nome: 'Prato do Céu',
        isFav: true,
        preço: 15,
        vendedor: 'Tio João',
    },
    {
        index: 4,
        foto: 'refeição',
        nome: 'Prato do PSG',
        isFav: true,
        preço: 15,
        vendedor: 'Tio João',
    },
]
