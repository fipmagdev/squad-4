import api from "../api";

export default{
    async getProdutos(){
        try{
            const response = api.get(`/Product`);
            return response;
        } catch(e){
            console.log(e);
        }
    },

    async getFavoritos(token: string){
        try{
            const response = api.get('/Favorite/:userId', {
                headers:{
                    Authorization: `Bearer ${token}`,
                }
            })
            return response;
        } catch(e){
            console.log(e);
        }
    },

    async criarProduto(token: string){
        try{
            const response = api.post(`/Product`, {}, {
                headers:{
                    Authorization: `Bearer ${token}`,
                }
            })
            return response;
        }catch(e){
            console.log(e);
        }
    },

    async releaseProduto(token: string, produtoId: number){
        try{
            const response = api.delete(`/Product/${produtoId}`, {
                headers:{
                    Authorization: `Bearer ${token}`,
                }
            })
            return response;
        }catch(e){
            console.log(e);
        }
    },
}