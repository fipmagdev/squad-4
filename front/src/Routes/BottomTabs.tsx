import * as React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { style } from '../global/global';
import { Image } from 'react-native';
import Profile from '../pages/Profile';
import Bandeja from '../pages/Bandeja';
import Home from '../pages/Home';
import CreateProduct from '../pages/CreateProduct';

function BottomTabs() {
    let icon: string;
    const Tab = createBottomTabNavigator();
    return (
        <>
            <Tab.Navigator initialRouteName='Home'
                screenOptions={{
                    tabBarStyle: { height: 100, backgroundColor: `${style.colors.secondaryRed}` },
                    tabBarShowLabel: false
                }}>
                <Tab.Screen
                    name="Perfil"
                    component={Profile}
                    options={{
                        headerShown: false,
                        tabBarIcon:()=>{
                            return <Image source={require('../../assets/userIcon.png')} style={{height:50,width:75}}/>
                            },
                    }}/>
                <Tab.Screen
                    name="Home"
                    component={Home}
                    options={{
                        headerShown: false,
                        tabBarIcon:()=>{
                            return <Image source={require('../../assets/menuIcon.png')} style={{height:50,width:75}}/>
                            }
                    }} />
                <Tab.Screen
                    name="Bandeja"
                    component={Bandeja}
                    options={{
                        headerShown: false,
                        tabBarStyle: { display: 'none' },
                        tabBarIcon:()=>{
                            return <Image source={require('../../assets/bandejaIcon.png')} style={{height:50,width:75}}/>
                            }
                    }} />
                <Tab.Screen
                    name="Adicionar"
                    component={CreateProduct}
                    options={{
                        headerShown: false,
                        tabBarIcon:()=>{
                            return <Image source={require('../../assets/adicionarIcon.png')} style={{height:50,width:75}}/>
                            }
                    }} />
            </Tab.Navigator>
        </>
    );
}

export default BottomTabs;
