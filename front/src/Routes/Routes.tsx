import * as React from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack'
import  OnBoarding1  from '../pages/OnBoarding1';
import  OnBoarding2  from '../pages/OnBoarding2';
import  OnBoarding3  from '../pages/OnBoarding3';
import { Login } from '../pages/LoginPage';
import { SingUp } from '../pages/SingUpPage';
import EsqueciMinhaSenha from "../pages/EsqueciMinhaSenha";
import BottomTabs from './BottomTabs';
import Splash from '../pages/Splash';
import MeusFavoritos from '../pages/MeusFavoritos';
import MeusProdutos from '../pages/MeusProdutos';
import Product from '../pages/Product';
import Bandeja from '../pages/Bandeja';
import SucessoCompra from '../pages/SucessoCompra';
import SucessoSenhaAlterada from '../pages/SucessoSenhaAlterada';

const Stack = createStackNavigator();

function Routes() {
    return (
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen
                    name="Splash" 
                    component={Splash}
                    options={{
                        headerShown: false
                    }}
                />
                <Stack.Screen
                    name="OnBoarding1" 
                    component={OnBoarding1}
                    options={{
                        headerShown: false
                    }}
                />
                <Stack.Screen
                    name="OnBoarding2" 
                    component={OnBoarding2}
                    options={{
                        headerShown: false
                    }}
                />
                <Stack.Screen
                    name="OnBoarding3" 
                    component={OnBoarding3}
                    options={{
                        headerShown: false
                    }}
                />
                <Stack.Screen
                    name="Login" 
                    component={Login}
                    options={{
                        headerShown: false
                    }}
                />
                <Stack.Screen
                    name="Cadastro" 
                    component={SingUp}
                    options={{
                        headerShown: false
                    }}
                />
                <Stack.Screen
                    name="EsqueciMinhaSenha" 
                    component={EsqueciMinhaSenha}
                    options={{
                        headerShown: false
                    }}
                />
                <Stack.Screen
                name="BottomTabs" 
                component={BottomTabs}
                options={{
                    headerShown: false
                }}
                />
                <Stack.Screen
                    name="MeusFavoritos" 
                    component={MeusFavoritos}
                    options={{
                        headerShown: false
                    }}
                />
                <Stack.Screen
                    name="MeusProdutos" 
                    component={MeusProdutos}
                    options={{
                        headerShown: false
                    }}
                />
                <Stack.Screen
                    name="Product" 
                    component={Product}
                    options={{
                        headerShown: false
                    }}
                />
                <Stack.Screen
                    name="Bandeja" 
                    component={Bandeja}
                    options={{
                        headerShown: false
                    }}
                />
                <Stack.Screen
                    name="SucessoCompra" 
                    component={SucessoCompra}
                    options={{
                        headerShown: false
                    }}
                />
                <Stack.Screen
                    name="SucessoSenhaAlterada" 
                    component={SucessoSenhaAlterada}
                    options={{
                        headerShown: false
                    }}
                />
            </Stack.Navigator>
        </NavigationContainer>

    );
}

export default Routes;