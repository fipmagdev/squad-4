import React, { useState } from 'react';
import { TouchableOpacity, View } from 'react-native';
import { Arrow, OrderConfig, OrderText, Title } from '../Bandeja/style';
import { BackButton } from '../CreateProduct/style';
import { BackContainer, Container, Data, DataInput, DataLabel, DataText, DataTitle, Duo, EditiIcon, ProfileOptions, Save, SaveButton, UserPhoto } from './style';
import { useNavigation } from "@react-navigation/native";

function Profile() {

    const [edit, setEdit] = useState(false);
    const [email, setEmail] = useState('');
    const [senha, setSenha] = useState('');
    const [cpf, setCpf] = useState('');
    const [rua, setRua] = useState('');
    const [bairro, setBairro] = useState('');
    const [estado, setEstado] = useState('');
    const [cep, setCep] = useState('');
    const navigation = useNavigation();

    return (
        <Container>
            <BackContainer>
                <TouchableOpacity>
                    <BackButton source={require('../../../assets/voltar.png')} />
                </TouchableOpacity>
            </BackContainer>

            <View>
                <UserPhoto source={require('../../../assets/usuario.png')} />
                <Title>Zequinha</Title>
            </View>

            <View>
                <Title>Dados</Title>
                <TouchableOpacity onPress={() => setEdit(true)} >
                    <EditiIcon source={require('../../../assets/edit.png')} />
                </TouchableOpacity>
            </View>

            <Data >
                <DataTitle>Informações pessoais</DataTitle>
                <Duo>
                    <DataLabel>email:</DataLabel>
                    {edit ? <DataInput

                        value={email}
                        onChange={(event: any) =>
                            setEmail(event?.target.value)} /> :
                        <DataText>{email}</DataText>}
                </Duo>

                <Duo>
                    <DataLabel>senha:</DataLabel>
                    {edit ? <DataInput
                        value={senha}
                        onChange={(event: any) =>
                            setSenha(event?.target.value)} /> :
                        <DataText>{senha}</DataText>}
                </Duo>

                <Duo>
                    <DataLabel>cpf:</DataLabel>
                    {edit ? <DataInput
                        value={cpf}
                        onChange={(event: any) =>
                            setCpf(event?.target.value)} /> :
                        <DataText>{cpf}</DataText>}
                </Duo>
            </Data>

            <Data>

                <DataTitle>Endereço</DataTitle>
                <Duo>
                    <DataLabel>Rua</DataLabel>
                    {edit ? <DataInput
                        value={rua}
                        onChange={(event: any) =>
                            setRua(event?.target.value)} /> :
                        <DataText>{rua}</DataText>}
                </Duo>

                <Duo>
                    <DataLabel>Bairro:</DataLabel>
                    {edit ? <DataInput
                        value={bairro}
                        onChange={(event: any) =>
                            setBairro(event?.target.value)} /> :
                        <DataText>{bairro}</DataText>}
                </Duo>

                <Duo>
                    <DataLabel>Estado:</DataLabel>
                    {edit ? <DataInput
                        value={estado}
                        onChange={(event: any) =>
                            setEstado(event?.target.value)} /> :
                        <DataText>{estado}</DataText>}
                </Duo>

                <Duo>
                    <DataLabel>email:</DataLabel>
                    {edit ? <DataInput
                        value={cep}
                        onChange={(event: any) =>
                            setCep(event?.target.value)} /> :
                        <DataText>{cep}</DataText>}
                </Duo>
                <SaveButton onPress={() => setEdit(false)}>
                    <Save source={require('../../../assets/salvar.png')} />
                </SaveButton>
            </Data>

            <ProfileOptions>
                <OrderConfig>
                    <OrderText onPress={() => navigation.navigate('MeusProdutos' as never)}>Meus produtos </OrderText>
                    <TouchableOpacity onPress={() => navigation.navigate('MeusProdutos' as never)}>
                        <Arrow source={require("../../../assets/arrow.png")} />
                    </TouchableOpacity>
                </OrderConfig>
                <OrderConfig>
                    <OrderText onPress={() => navigation.navigate('MeusFavoritos' as never)}>Favoritos</OrderText>
                    <TouchableOpacity onPress={() => navigation.navigate('MeusFavoritos' as never)}>
                        <Arrow source={require("../../../assets/arrow.png")} />
                    </TouchableOpacity>
                </OrderConfig>
            </ProfileOptions>
        </Container>
    );
}

export default Profile;