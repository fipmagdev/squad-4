import styled from 'styled-components/native';
import { style } from '../../global/global';

export const Container = styled.View`
    flex: 1;
    padding: 20px;
`

export const UserPhoto = styled.Image`
    width: 80px;
    height: 80px;
    align-self: flex-start;

`

export const EditiIcon = styled.Image`
    width: 32px;
    height: 32px;
    position: relative;
    align-self: flex-end;
    margin-top: 41;
    margin-right: 46;
`

export const Data = styled.View`
    margin-top: 40px;
    align-self: flex-start;
    margin-left: 15%;
    position: relative;
    width: 70%;
`

export const DataTitle = styled.Text`
    font-size: 24px;
    font-family:${style.fonts.r700};
    margin-bottom: 10px;
`

export const DataText = styled.Text`
    font-size: 20px;
    font-family:${style.fonts.r700};
    margin-top: 5px;
    width: 80%;
    
`

export const DataLabel = styled.Text`
    font-size: 20px;
    font-family:${style.fonts.r700};
    margin-top: 5px;
`

export const ProfileOptions = styled.View`
    margin-top: 100px;

`

export const DataInput= styled.TextInput`
    font-size: 20px;
    font-family:${style.fonts.r700};
    width: 80%;
    margin-top: 5px;
`
export const Duo = styled.View`
    flex-direction: row;
    align-items: center;
    gap: 10px;
`

export const BackContainer = styled.View`
    padding-bottom: 20px;
`
export const Save = styled.Image`
    width: 32px;
    height: 32px;
    margin-top: 20px;
    align-self: center;
`

export const SaveButton = styled.TouchableOpacity`
    position: absolute;
    align-self: flex-end;
    bottom: -50px;
`