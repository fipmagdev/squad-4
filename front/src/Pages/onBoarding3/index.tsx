import { Main, TitleOnBoarding, TextOnBoarding, Dots, Dot,YellowDot, BackImage, ImageOnBoarding } from "./style";
import { useNavigation } from "@react-navigation/native";

const OnBoarding3 = ()=> {

    const navigation = useNavigation();

    return(
        <Main onPress={() => navigation.navigate('Login' as never)}>

            <BackImage><ImageOnBoarding source={require('../../../assets/PicMoto.png')}/></BackImage>
            
            <TitleOnBoarding>ENTREGA ÁGIL</TitleOnBoarding>
            
            <TextOnBoarding>DeBandeja possui a entrega mais rápida dentre a concorrência</TextOnBoarding>
            
            <Dots>
                <Dot/>
                <Dot/>
                <YellowDot/>
            </Dots>

        </Main>
    )
}

export default OnBoarding3;