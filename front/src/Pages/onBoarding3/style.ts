import styled from 'styled-components/native'

export const Main = styled.TouchableOpacity`
    display: flex;
    width: 100%;
    height: 100%;
    justify-content: space-between;
    align-items: center;
    flex-direction: column;
    background-color: #FC0505;
`;

export const TitleOnBoarding = styled.Text`
    color: white;
    font-size: 40px;
    font-weight: bold;
`;

export const TextOnBoarding = styled.Text`
    color: white;
    font-size: 25px;
    width: 345px;
    text-align: center;
`
export const Dots = styled.View`
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
`;

export const Dot = styled.View`
    width: 10px;
    height: 10px;
    background-color: white;
    margin: 0 10px 30px 10px;
    border-radius: 10px
`;

export const BackImage = styled.View`
    width: 100%;
    height: 535px;
    background-color: white;
    padding-top: 20px;
`;

export const YellowDot = styled.View`
    width: 10px;
    height: 10px;
    background-color:#FF9900;
    margin: 0 10px 30px 10px;
    border-radius: 10px
`;

export const ImageOnBoarding = styled.Image`
    width: 393px;
    height: 400px;
    align-self: center; 
    margin-top: 25px;
    margin-bottom: 85px;
`;