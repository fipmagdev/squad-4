import { Main, TitleOnBoarding, TextOnBoarding, Dots, Dot,YellowDot, BackImage, ImageOnBoarding } from "./style";
import { useNavigation } from "@react-navigation/native";

const OnBoarding2 = ()=> {

    const navigation = useNavigation();
    
    return(
        <Main onPress={() => navigation.navigate('OnBoarding3' as never)}>

            <BackImage><ImageOnBoarding source={require('../../../assets/mapaOnBoarding.png')}/></BackImage>
            
            <TitleOnBoarding>ENTREGA NACIONAL</TitleOnBoarding>
            
            <TextOnBoarding>DeBandeja já está disponível em todos as regiões do Brasil</TextOnBoarding>
            
            <Dots>
                <Dot/>
                <YellowDot/>
                <Dot/>
            </Dots>

        </Main>
    )
}

export default OnBoarding2;