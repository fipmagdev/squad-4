import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import { BackButton, MainContainer, ProdCaracteristcs, ProductBackground, ProductPhoto, ProductTitle, Title, SecundaryContainer, SubmitText, SubmitButton, TextError } from './style';
import { useForm, Controller} from 'react-hook-form'


function CreateProduct() {


    const { control, handleSubmit, formState: { errors } } = useForm( { mode: 'onTouched'} );

    const onSubmit = (data: any) =>{
        console.log(data)
    }

    return ( 
        <MainContainer>
            <SecundaryContainer>
                <View>
                    <TouchableOpacity>
                        <BackButton source={require('../../../assets/voltar.png')} />
                    </TouchableOpacity>
                    <Title>CRIE SEU PRODUTO</Title>
                </View>
                <View>
                <Controller
                        control={control}
                        name='nomeProduto'
                        render={({field:{onBlur, onChange, value}}) => (
                            <ProdCaracteristcs
                                onChangeText={onChange}
                                onBlur={onBlur}
                                value={value}
                                placeholder = 'Nome do produto'
                            />                            
                        )}
                        rules={{
                            required: 'Este campo é obrigatório.',
                        }}
                        defaultValue=''                                                
                    />
                    { errors.nomeProduto && <TextError>{errors.nomeProduto.message}</TextError> }


                <Controller
                        control={control}
                        name='caracteristicasPoduto'
                        render={({field:{onBlur, onChange, value}}) => (
                            <ProdCaracteristcs
                                onChangeText={onChange}
                                onBlur={onBlur}
                                value={value}
                                placeholder = 'Características do produto'
                            />                            
                        )}
                        rules={{
                            required: 'Este campo é obrigatório.',
                        }}
                        defaultValue=''
                    />                    
                    { errors.caracteristicasPoduto && <TextError>{errors.caracteristicasPoduto.message}</TextError> }

                <Controller
                        control={control}
                        name="valorProduto"
                        render={({field:{onBlur, onChange, value}}) => (
                            <ProdCaracteristcs
                                onChangeText={onChange}
                                onBlur={onBlur}
                                value={value}
                                placeholder = "Valor do produto"
                            />                            
                        )}
                        rules={{
                            required: 'Este campo é obrigatório.',
                        }}
                        //defaultValue=''                         
                    />
                    { errors.valorProduto && <TextError>{errors.valorProduto.message}</TextError> }


                </View>
                <View>
                    <ProductTitle>Imagem do produto</ProductTitle>
                    <ProductBackground>
                        <ProductPhoto source={require('../../../assets/prato1.png')} />
                    </ProductBackground>

                    <SubmitButton onPress={handleSubmit(onSubmit)} >
                        <SubmitText>Adicionar</SubmitText>
                    </SubmitButton>
                </View>
            </SecundaryContainer>

        </MainContainer>
    );
}

export default CreateProduct;