import styled from 'styled-components/native';
import { style } from '../../global/global';

export const MainContainer = styled.View`
    flex: 1;
`
export const SecundaryContainer = styled.View`
    padding: 20px;
`

export const BackButton = styled.Image`
    width: 35px;
    height: 35px;
`

export const Title = styled.Text`
    font-family:${style.fonts.r700};
    font-size: 35px;
    align-self: center;
    color:${style.colors.mainBlack};
    margin-top: 27px;
`

export const ProdCaracteristcs = styled.TextInput`
    width: 386px;
    height: 60px;
    font-family:${style.fonts.r700};
    font-size: 20px;
    border: 1.5px solid ${style.colors.mainBlack};
    border-radius:10;
    color:${style.colors.mainBlack};
    margin-top: 39px;
    margin-bottom: 10px;
` 

export const ProductTitle = styled.Text`
    font-family:${style.fonts.r700};
    font-size: 25px;
<<<<<<< HEAD
    font-family: 'Roboto';
    margin-top: 50px;
=======
    color:${style.colors.mainBlack};
    margin-top: 49px;
>>>>>>> 965b5dfd7ddcfbd8216e4cb12591e808e161d604
` 

export const ProductBackground = styled.View`
    background-color: ${style.colors.mainGrey};
    border-radius: 5px;
    width: 388px;
    height: 200px;
    flex-direction:column;
    justify-content:center;
    margin-top: 10px;
`

export const ProductPhoto = styled.Image`
    width: 150px;
    height: 150px;
    border-radius: 5px;
    align-self:center;
    
`

export const Footer = styled.View`
    background-color: red;
    width: 100%;
    height: 100px;
    flex-direction: row;
    align-items: center;
    justify-content: space-evenly;
    position: absolute;
    bottom: 0;
`

export const FooterIcons = styled.Image`
    width: 75px;
    height: 50px;
`

export const SubmitButton = styled.TouchableOpacity`
    width: 385px;
    height: 61px;
    background-color: red;
    border-radius: 5px;
    margin-top: 15px;
    align-self: center;
    padding-top: 15px;
`

export const SubmitText = styled.Text`
    color: white;
    font-size: 24px;
    font-family: 'Roboto';
    font-weight: bold;
    align-self: center;
`
export const TextError = styled.Text`
    font-size: 15px;
    font-family: 'Roboto';
    font-weight: bold;
    color: red;
    
`

