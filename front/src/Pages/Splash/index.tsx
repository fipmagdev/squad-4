import React, { useEffect } from 'react'
import { Container, Logo, Title } from './style';
import { useNavigation } from "@react-navigation/native";

function Splash() {

    const navigation = useNavigation();

    useEffect(()=>{setTimeout(() => {navigation.navigate('OnBoarding1' as never)}, 3000);},[])

    return ( 
        <Container>
            <Logo source={require('../../../assets/LogoBandeja.png')} />
            <Title>DeBandeja</Title>
        </Container>
    );
}

export default Splash;