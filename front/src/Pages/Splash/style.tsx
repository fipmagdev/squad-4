import styled from 'styled-components/native';
import { style } from '../../global/global';

export const Container = styled.View`
    flex: 1;
    background-color: ${style.colors.mainRed};
    flex-direction: column;
    padding-top: 170px;
    gap: 20px;
`

export const Logo = styled.Image`
    width: 234.4;
    height: 234.4;
    margin-left: 75px;
`
export const Title = styled.Text`
    font-family:${style.fonts.r700};
    font-size:40px;
    color: ${style.colors.mainWhite};
    align-self: center;
`