import styled from "styled-components/native";
import { style } from "../../global/global";

export const MainSingUp = styled.ScrollView`
    background-color:${style.colors.mainRed};
    width: 100%;
    height: 100%;
    overflow-y: scroll;
`;

export const MiniImageBox = styled.TouchableOpacity`
    width: 64px;
    height: 64px;
    align-self:center;
    margin-top:13px;
`;

export const MiniImage = styled.Image`
    width: 64px;
    height: 64px;
`;

export const  MiniInput = styled.TextInput`
    width: 100%;
    height: 50px;
    font-family:${style.fonts.r700};
    padding-left: 5px;
    background-color: ${style.colors.mainWhite};
    text-align: center;
    border-radius: 5px;
    margin-top:10px;
`;

export const  ViewMiniInptus = styled.View`
    display: flex;
    width: 384px;
    height: 106px;
    flex-direction: row;
    justify-content:space-between;
    align-self:center;
    margin-top:11px;
`;

export const  BoxMiniInput = styled.View`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    width: 32.5%;
`;

export const  TitleMiniInput = styled.Text`
    color: ${style.colors.mainWhite};
    font-size: 25px;
`;

export const SingUpLoginButton = styled.TouchableOpacity`
    width: 385px;
    height: 61px;
    background-color: ${style.colors.mainWhite};
    margin:  20px auto;
    display: flex;
    justify-content: center;
    align-items: center;
    border-radius: 5px;
`;

export const SingUpTextButton = styled.Text`
    color: ${style.colors.mainRed};
    font-family:${style.fonts.r700};
    font-size: 25px;
`;

export const InputForm = styled.TextInput`
    width: 386px;
    height: 60px;
    font-family:${style.fonts.r700};
    font-size: 20px;
    border: 1.5px solid ${style.colors.mainWhite};
    border-radius:10;
    color:${style.colors.mainWhite};
    margin-top: 39px;
    margin-bottom: 10px;
` 
export const Input = styled.TextInput`
    width: 100%;
    font-family:${style.fonts.r700};
    padding-left: 15px;
    background-color: white;
    border-radius: 5px;
    height: 61px;
    font-size: 20px;
    margin-top:11px;
`;

export const TitleInput = styled.Text`
    color: white;
    font-size: 30px;
`;


export const InputBox = styled.View`
    width: 385px;
    margin-top:11px;
    align-self:center;
`;

export const TextError = styled.Text`
    font-family:${style.fonts.r700};
    font-size:18px;
    color:${style.colors.mainWhite};
    margin-top:5px;
    margin-left:15px;
`


export const MiniTextError = styled.Text`
    font-family:${style.fonts.r700};
    font-size:10px;
    color:${style.colors.mainWhite};
`