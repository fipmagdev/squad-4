import { MainSingUp, MiniImage, MiniInput, ViewMiniInptus, BoxMiniInput, TitleMiniInput, SingUpLoginButton, SingUpTextButton, InputBox, TitleInput, Input, MiniImageBox, TextError, MiniTextError, } from './style'
import { useNavigation } from "@react-navigation/native";
import { useForm, Controller } from 'react-hook-form';

interface  FormData {
    nome: string,
    email: string,
    cpf: string,
    senha: string,
    endereco: string,
    estado: string,
    bairro: string,
    cep: string
}

export function SingUp(){

    const navigation = useNavigation();

    const { control, handleSubmit, formState:{errors}, getValues } = useForm<FormData>({mode: "onTouched"});

    const onSubmit = (data: FormData) => {
        console.log(data);
    }


    return(
        <MainSingUp>
            <MiniImageBox onPress={() => navigation.navigate('Login' as never)}>
                <MiniImage source={require('../../../assets/miniLogo.png')}/>
            </MiniImageBox>

            <InputBox>
                <TitleInput>Nome</TitleInput>
                <Controller
                    control={control}
                    name='nome'
                    defaultValue=''
                    render={({field:{onBlur, onChange, value}}) => (
                        <Input
                            onBlur={onBlur}
                            onChangeText={(value:any) => onChange(value)}
                            value={value}
                            maxLength={35}
                        />
                    )}
                    rules={{
                        required: "Nome é obrigatório",
                        pattern: {
                            value: /^[a-zA-Z0-9]+$/i,
                            message: 'Somente letras e números permitidos'
                        },
                    }}
                />
                {errors.nome && <TextError>{errors.nome.message}</TextError>}
            </InputBox>

            <InputBox>
                <TitleInput>Email</TitleInput>
                <Controller
                    control={control}
                    name='email'
                    defaultValue=''
                    render={({field:{onBlur, onChange, value}}) => (
                        <Input
                            onBlur={onBlur}
                            onChangeText={(value:any) => onChange(value)}
                            value={value}
                            maxLength={256}
                        />
                    )}
                    rules={{
                        required: "Email é obrigatório",
                        pattern: {
                            value: /^[A-Z0-9._-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                            message: "Formato de email inválido"
                        },
                    }}
                />
                {errors.email && <TextError>{errors.email.message}</TextError>}
            </InputBox>

            <InputBox>
                <TitleInput>Senha</TitleInput>
                <Controller
                    control={control}
                    name='senha'
                    defaultValue=''
                    render={({field:{onBlur, onChange, value}}) => (
                        <Input
                            secureTextEntry={true}
                            onBlur={onBlur}
                            onChangeText={(value:any) => onChange(value)}
                            value={value}
                            maxLength={18}
                        />
                    )}
                    rules={{
                        required: "Senha é obrigatória",
                        minLength: {
                            value: 6,
                            message: 'Senha muito curta'
                        },
                    }}
                />
                {errors.senha && <TextError>{errors.senha.message}</TextError>}
            </InputBox>

            <InputBox>
                <TitleInput>CPF</TitleInput>
                <Controller
                    control={control}
                    name='cpf'
                    defaultValue=''
                    render={({field:{onBlur, onChange, value}}) => (
                        <Input
                            type={"cpf"}
                            onBlur={onBlur}
                            onChangeText={(value:any) => onChange(value)}
                            value={value}
                        />
                    )}
                    rules={{
                        required: "CPF é obrigatório",
                        minLength: {
                            value: 11,
                            message: 'CPF inválido'
                        },
                    }}
                />
                {errors.cpf && <TextError>{errors.cpf.message}</TextError>}
            </InputBox>

            <InputBox>
                <TitleInput>Endereço</TitleInput>
                <Controller
                    control={control}
                    name='endereco'
                    defaultValue=''
                    render={({field:{onBlur, onChange, value}}) => (
                        <Input
                            onBlur={onBlur}
                            onChangeText={(value:any) => onChange(value)}
                            value={value}
                            maxLength={50}
                        />
                    )}
                    rules={{
                        required: "Endereço é obrigatório",
                        pattern: {
                            value: /^[a-zA-Z0-9]+$/i,
                            message: 'Somente letras e números permitidos'
                        },
                    }}
                />
                {errors.endereco && <TextError>{errors.endereco.message}</TextError>}
            </InputBox>

            <ViewMiniInptus>
                <BoxMiniInput>
                    <TitleMiniInput>Estado</TitleMiniInput>
                    <Controller
                    control={control}
                    name='estado'
                    defaultValue=''
                    render={({field:{onBlur, onChange, value}}) => (
                        <MiniInput
                            onBlur={onBlur}
                            onChangeText={(value:any) => onChange(value)}
                            value={value}
                            maxLength={35}
                        />
                    )}
                    rules={{
                        required: "Estado é obrigatório",
                        pattern: {
                            value: /^[a-zA-Z]+$/i,
                            message: 'Somente letras são permitidas'
                        },
                    }}
                />
                {errors.estado && <MiniTextError>{errors.estado.message}</MiniTextError>}
                </BoxMiniInput>

                <BoxMiniInput>
                    <TitleMiniInput>Bairro</TitleMiniInput>
                    <Controller
                    control={control}
                    name='bairro'
                    defaultValue=''
                    render={({field:{onBlur, onChange, value}}) => (
                        <MiniInput
                            onBlur={onBlur}
                            onChangeText={(value:any) => onChange(value)}
                            value={value}
                            maxLength={35}
                        />
                    )}
                    rules={{
                        required: "Bairro é obrigatório",
                        pattern: {
                            value: /^[a-zA-Z]+$/i,
                            message: 'Somente letras são permitidas'
                        },
                    }}
                />
                {errors.bairro && <MiniTextError>{errors.bairro.message}</MiniTextError>}
                </BoxMiniInput>

                <BoxMiniInput>
                    <TitleMiniInput>CEP</TitleMiniInput>
                    <Controller
                    control={control}
                    name='cep'
                    defaultValue=''
                    render={({field:{onBlur, onChange, value}}) => (
                        <MiniInput
                            onBlur={onBlur}
                            onChangeText={(value:any) => onChange(value)}
                            value={value}
                            maxLength={35}
                        />
                    )}
                    rules={{
                        required: "Cep é obrigatório",
                        minLength: {
                            value: 9,
                            message: 'Cep inválido'
                        },
                    }}
                />
                {errors.cep && <MiniTextError>{errors.cep.message}</MiniTextError>}
                </BoxMiniInput>
            </ViewMiniInptus>

            <SingUpLoginButton onPress={handleSubmit(onSubmit)}>
                    <SingUpTextButton>Cadastrar</SingUpTextButton>
            </SingUpLoginButton> 

        </MainSingUp>

    );

}
