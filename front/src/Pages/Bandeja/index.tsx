import React, { useState } from 'react';
import { View, TouchableOpacity } from 'react-native';
import { BackButton } from '../CreateProduct/style';
import { Container, PostText, DishDescription, RestaunrantIcon, Title, TrashIcon, RestaurantInfo, DishIcon, DishInfo, Quantity, ButtonText, CompleteOrder, OrderConfig, Arrow, OrderText, OrderButton, ButtonOrderText } from './style';
import { useNavigation } from "@react-navigation/native";

function Bandeja() {

    const [value, setValue] = useState(1);
    function incrementValue(value: number) {
        setValue(value + 1);
    }
    function decrementValue(value: number) {
        if (value > 1)
        setValue(value - 1);
    }
    const navigation = useNavigation();

    return ( 
        <Container>
            <View>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                        <BackButton source={require('../../../assets/voltar.png')} />
                </TouchableOpacity>
                <Title><strong>Bandeja</strong></Title>
                <TouchableOpacity>
                    <TrashIcon source={require('../../../assets/lixeira.png')}/>
                </TouchableOpacity>
            </View>

            <RestaurantInfo>
                <RestaunrantIcon source={require("../../../assets/restaurante1.png")} />
                <PostText>Restaurante do biru</PostText>
            </RestaurantInfo>

            <DishInfo>
                <DishIcon source={require("../../../assets/prato1.png")} />
                <DishDescription>
                    <PostText>Lagosta ao molho tega</PostText>
                    <PostText>R$ 99,12</PostText>
                    <Quantity>
                        <TouchableOpacity onPress={() => decrementValue(value)} >
                            <ButtonText >-</ButtonText>
                        </TouchableOpacity>
                        <PostText>{ value }</PostText>
                        <TouchableOpacity onPress={() => incrementValue(value)}>
                            <ButtonText  >+</ButtonText>
                        </TouchableOpacity>
                    </Quantity>   
                </DishDescription>
            </DishInfo>

            <CompleteOrder>
                <OrderConfig>
                    <OrderText>Selecionar método de pagamento </OrderText>
                    <TouchableOpacity>
                        <Arrow source={require("../../../assets/arrow.png")} />
                    </TouchableOpacity>
                </OrderConfig>
                <OrderConfig>
                    <OrderText>Endereço</OrderText>
                    <TouchableOpacity>
                        <Arrow source={require("../../../assets/arrow.png")} />
                    </TouchableOpacity>
                </OrderConfig>
                <OrderConfig>
                    <OrderText>Total a pagar</OrderText>
                    <OrderText>R$ 99,12</OrderText>
                </OrderConfig>
            </CompleteOrder>
            <OrderButton onPress={() => navigation.navigate('StatusPedido' as never)}>    
                <ButtonOrderText>Concluir pagamento</ButtonOrderText>
            </OrderButton>
        </Container>


    );
}

export default Bandeja;