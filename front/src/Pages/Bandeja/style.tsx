import styled from 'styled-components/native';
import { style } from '../../global/global';

export const Container = styled.View`
    flex: 1;
    flex-direction: column;
    padding-left: 18px;
    padding-top: 18px;
`

export const Title = styled.Text`
    font-size: 32;
    font-family:${style.fonts.r700};
    position: absolute;
    align-self: center;
    margin-top: 25px;
`

export const TrashIcon = styled.Image`
    width: 32px;
    height: 32px;
    position: relative;
    align-self: flex-end;
    margin-right: 46;
`

export const RestaurantInfo = styled.View`
    flex-direction: row;
    align-items: center;
    margin-left: 4.2%;
    margin-top: 35px;
    gap: 15px;
`

export const RestaunrantIcon = styled.Image`
    width: 80px;
    height: 80px;
`

export const PostText = styled.Text`
    font-size: 24;
    font-family:${style.fonts.r700};
`

export const DishInfo = styled.View`
    flex-direction: row;
    align-items: center;
    margin-left: 4.2%;
    gap: 15px;

`

export const DishDescription = styled.View`
    flex-direction: column;
    align-items: left;
    margin-top: 25px;
    gap: 15px;
`

export const DishIcon = styled.Image`
    width: 90px;
    height: 90px;
`

export const Quantity = styled.View`
    width: 143;
    height: 52;
    border-width: 1;
    border-color: black;
    border-radius: 5px;
    flex-direction: row;
    align-items: center;
    justify-content: space-around;
`

export const ButtonText = styled.Text`
    color: red;
    font-size: 52px;
    font-family:${style.fonts.r700};
`

export const CompleteOrder = styled.View`
    flex-direction: column;
    padding-left: 25px;
    padding-right: 25px;
    margin-top: 170px;
`

export const OrderConfig = styled.View`
    flex-direction: row;
    align-items: center;
    justify-content: space-between;
    margin-top: 10px;
    padding-bottom: 20px;
    border-bottom-width: 1;
`

export const OrderText = styled.Text`
    font-size: 24px;
    font-family:${style.fonts.r700};
    margin-top: 20px;
`

export const Arrow = styled.Image`
    width: 20px;
    height: 20px;
    align-self: flex-end;
    margin-top: 25px;
`

export const OrderButton = styled.TouchableOpacity`
    width: 385px;
    height: 61px;
    background-color: red;
    border-radius: 5px;
    margin-top: 25px;
    align-self: center;
    padding-top: 15px;
`

export const ButtonOrderText = styled.Text`
    color: white;
    font-size: 24px;
    font-family: 'Roboto';
    font-weight: bold;
    align-self: center;
`