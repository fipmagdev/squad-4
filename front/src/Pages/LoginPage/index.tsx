import { MainLogin, ImageBrand, TitleLogin, Formulario, Input, InputBox, TitleInput, LoginButton, Buttons, TextButton, SingUp, PwForgot, AreaText, TextError } from "./style"
import { useNavigation } from "@react-navigation/native";
import { useForm, Controller } from 'react-hook-form';
import AsyncStorage from '@react-native-async-storage/async-storage';
import usuarioServices from "../../services/usuarioServices";
import { AuthContext } from "../../contexts/auth";
import React from "react";
import api from "../../services/api";

export interface LoginData {
    email: string,
    password: string
}

export function Login (){

    const Auth = React.useContext(AuthContext);

    const { control, handleSubmit, formState:{errors}, getValues } = useForm<any>({mode: "onTouched"});

    const onSubmit = (data: any) => {
        api.post('/login', data).then(response => {
            const token = response.data.token;
            AsyncStorage.setItem('token', token);
            alert('Login feito com sucesso!');
            navigation.navigate('BottomTabs' as never);
        },
        (error => ('Login não pode ser concluído.')))
    };
    
    const navigation = useNavigation();

    return(
        <MainLogin>

            <ImageBrand source={require('../../../assets/LogoBandeja.png')}/>
            <TitleLogin>DeBandeja</TitleLogin>
            
            <Formulario>
                <InputBox>
                    <TitleInput>Email</TitleInput>
                    <Controller
                        control={control}
                        name='email'
                        defaultValue=''
                        render={({field:{onBlur, onChange, value}}) => (
                            <Input
                                onBlur={onBlur}
                                onChangeText={(value:any) => onChange(value)}
                                value={value}
                                maxLength={256}
                            />
                        )}
                        rules={{
                            required: "Email é obrigatório",
                            pattern: {
                                value: /^[A-Z0-9._-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                                message: "Formato de email inválido"
                            },
                        }}
                    />
                    {errors.email && <TextError>{errors.email.message}</TextError>}
                </InputBox>

                <InputBox>
                    <TitleInput>Senha</TitleInput>
                    <Controller
                        control={control}
                        name='password'
                        defaultValue=''
                        render={({field:{onBlur, onChange, value}}) => (
                            <Input
                                secureTextEntry={true}
                                onBlur={onBlur}
                                onChangeText={(value:any) => onChange(value)}
                                value={value}
                                maxLength={18}
                            />
                        )}
                        rules={{
                            required: "Senha é obrigatória",
                            minLength: {
                                value: 6,
                                message: 'Senha muito curta'
                            },
                        }}
                    />
                    {errors.senha && <TextError>{errors.senha.message}</TextError>}
                </InputBox>
            </Formulario>

            <AreaText>
                <SingUp onPress={() => navigation.navigate('Cadastro' as never)}>Cadastrar / </SingUp>
                <PwForgot onPress={() => navigation.navigate('EsqueciMinhaSenha' as never)}>Esqueci minha senha</PwForgot>
            </AreaText>
            
            <Buttons>
                <LoginButton onPress={handleSubmit(onSubmit)}>
                    <TextButton>Logar</TextButton>
                </LoginButton>

                <LoginButton onPress={handleSubmit(onSubmit)}>
                    <TextButton>Entrar como visitante</TextButton>
                </LoginButton>                
            </Buttons>
            
        </MainLogin>
    )
}