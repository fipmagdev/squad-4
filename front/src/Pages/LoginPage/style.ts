import styled from "styled-components/native";
import { style } from '../../global/global';

export const MainLogin = styled.View`
    background-color: ${style.colors.mainRed};
    width: 100%;
    height: 100%;
`;

export const ImageBrand = styled.Image`
    width: 293px;
    height: 293px;
    margin: 0 auto;
`;

export const TitleLogin = styled.Text`
    font-family:${style.fonts.r700};
    font-size: 40px;
    color: white;
    text-align: center;
`;

export const Formulario = styled.View`
    width: 385px;
    height: 215px;
    margin-top: 24px;
    margin-bottom:30px;
    align-self: center;
    flex-direction: column;
    justify-content: space-between;
`

export const Input = styled.TextInput`
    font-family:${style.fonts.r700};
    width: 100%;
    background-color: ${style.colors.mainWhite};
    border-radius: 5px;
    height: 61px;
    padding-left: 15px;
    font-size: 20px;
`;

export const TitleInput = styled.Text`
    height: 34px;
    color: ${style.colors.mainWhite};
    font-size: 20px;
`;

export const InputBox = styled.View`
    width: 385px;
`;

export const LoginButton = styled.TouchableOpacity`
    width: 385px;
    height: 61px;
    background-color: ${style.colors.mainWhite};
    margin:  8px auto;
    display: flex;
    justify-content: center;
    align-items: center;
    border-radius: 5px;
`;

export const Buttons = styled.View`
    margin-top: 60px;
`;

export const TextButton = styled.Text`
    font-family:${style.fonts.r700};
    color: ${style.colors.mainRed};
    font-size: 20px;

`;

export const SingUp = styled.Text`
    font-family:${style.fonts.r700};
    color: ${style.colors.mainWhite};
    font-size: 20px;
`;

export const PwForgot = styled.Text` 
    font-family:${style.fonts.r700};
    color: ${style.colors.mainWhite};
    font-size: 20px;
`;

export const AreaText = styled.View`
    display: flex;
    flex-direction: row;
    justify-content: center;
`;

export const TextError = styled.Text`
    font-family:${style.fonts.r700};
    font-size:18px;
    color:${style.colors.mainWhite};
    margin-top:5px;
    margin-left:15px;
`