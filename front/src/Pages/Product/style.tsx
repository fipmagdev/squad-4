import styled from 'styled-components/native';

export const MainContainer = styled.View`
    flex: 1;
    background-color: red;
`
export const Container = styled.View`
    flex: 1;
    background-color: red;
    padding: 20px;
    flex-direction: row;
    justify-content: space-between;
`
export const Container2 = styled.View`
    background-color: white;
    height: 70%;
    border-top-left-radius: 50px;
    border-top-right-radius: 50px;

`
export const BackButton = styled.Image`
    width: 35px;
    height: 35px;
`

export const ProductPhoto = styled.Image`
    width: 321;
    height: 250;
    margin-top: -160;
    align-self: center;

`

export const Title = styled.Text`
    font-size: 45px;
    font-family: 'Roboto';
    font-weight: bold;
    align-self: center;
    margin-top: 50px;
`

export const Price = styled.Text`
    font-size: 30px;
    font-family: 'Roboto';
    font-weight: bold;
    color: red;
    margin-top: 25px;
`

export const PriceNum = styled.Text`
    font-size: 30px;
    font-family: 'Roboto';
    font-weight: bold;
    margin-top: 25px;
`

export const TextView = styled.View`
    flex-direction: row;
    gap: 5px;
    align-self: center;
`

export const View3 = styled.View`
    flex-direction: row;
    justify-content: space-between;
    margin-left: 50px;
    margin-right: 50px;
    margin-top:30px;
`

export const View3Image = styled.Image`
    width: 25px;
    height: 25px;
    margin-top: 25px;
`

export const View3Text = styled.Text`
    font-size: 20px;
    font-family: 'Roboto';
    font-weight: bold;
    margin-top: 25px;

`

export const Duo = styled.View`
    flex-direction: row;
    align-items: center;
    gap: 5px;
`

export const DishDescription = styled.Text`
    font-size: 25px;
    font-family: 'Roboto';
    font-weight: bold;
    margin-left: 50px;
    margin-right: 50px;
`
export const View4 = styled.View`
    flex-direction: column;
    align-items: center;
    gap: 30px;
    margin-top: 39px;
`


