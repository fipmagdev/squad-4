import React, { useState } from "react";
import { TouchableOpacity, View } from 'react-native';
import { ButtonOrderText, OrderButton } from '../Bandeja/style';
import { BackButton, DishDescription, Duo, Container, Container2, Price, PriceNum, ProductPhoto, TextView, Title, View3, View3Image, View3Text, View4, MainContainer, } from './style';
import { useNavigation } from "@react-navigation/native";

function Product() {
    const [fav, setFav]=useState<Boolean>(false);

    const navigation = useNavigation();

    return ( 
        <MainContainer>
        
            <Container>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                    <BackButton source={require('../../../assets/voltarBranco.png')} />
                </TouchableOpacity>
                <TouchableOpacity onPress={() => setFav(!fav)}>
                        {fav?
                            (<BackButton source={require('../../../assets/coraçãoBranco.png')}/>)
                            :(<BackButton source={require('../../../assets/coraçãoVazioBranco.png')} />)
                        }
                </TouchableOpacity>
            </Container>

            <Container2>
                <ProductPhoto source={require('../../../assets/inspecPrato.png')} />
                <Title>Prato da casa</Title>

                <TextView>
                    <Price><i>R$</i></Price>
                    <PriceNum><i>15,90</i></PriceNum>
                </TextView>

                <View3>
                    <Duo>
                        <View3Image source={require('../../../assets/estrela.png')} />
                        <View3Text>5,0</View3Text>
                    </Duo>

                    <Duo>
                        <View3Image source={require('../../../assets/relogio.png')} />
                        <View3Text>30-45 Min</View3Text>
                    </Duo>
                </View3>

                    <View4>
                        <DishDescription>Arroz soltinho com feijão feito na hora, uma salada supimapa e um bife de dar água na boca.</DishDescription>
                        <OrderButton onPress={() => navigation.navigate('Bandeja' as never)}>
                            <ButtonOrderText>Adicionar à bandeja</ButtonOrderText>
                        </OrderButton>
                    </View4>
            </Container2>
            
    
        </MainContainer>
     );
}

export default Product;