import { Main, TitleOnBoarding, TextOnBoarding, Dots, Dot,YellowDot, BackImage, ImageOnBoarding } from "./style";
import { useNavigation } from "@react-navigation/native";

const OnBoarding1 = ()=> {

    const navigation = useNavigation();

    return(
        <Main onPress={() => navigation.navigate('OnBoarding2' as never)}>

            <BackImage><ImageOnBoarding source={require('../../../assets/Celular.png')}/></BackImage>
            
            <TitleOnBoarding>MONTE O PEDIDO</TitleOnBoarding>
            
            <TextOnBoarding>Aproveite a enorme variedade de deliciosas refeições e bebidas disponíveis!</TextOnBoarding>
            
            <Dots>
                <YellowDot/>
                <Dot/>
                <Dot/>
            </Dots>

        </Main>
    )
}

export default OnBoarding1;