export const style = {
    colors: {
        mainRed:'#FC0505',
        secondaryRed:'#FF0000',
        terciaryRed:'#E50914',
        mainWhite:'#FFFFFF',
        mainBlack:'#000000',
        mainGrey:'#D9D9D9',
        secondaryGrey:'#737070',
    },
    fonts: {
        r300: "Roboto_300Light",
        r400: "Roboto_400Regular",
        r500: "Roboto_500Medium",
        r700: "Roboto_700Bold",
        r900: "Roboto_900Black"

    }
}
