import React from 'react';
import { useFonts, Roboto_300Light, Roboto_400Regular, Roboto_500Medium, Roboto_700Bold, Roboto_900Black } from "@expo-google-fonts/roboto";
import AppLoading from "expo-app-loading";
import Routes from './src/Routes/Routes';
import AuthProvider from './src/contexts/auth';

export default function App() {
  let [fontsLoaded, error] = useFonts({ Roboto_300Light, Roboto_400Regular, Roboto_500Medium, Roboto_700Bold, Roboto_900Black })
  if (!fontsLoaded) {
    return <AppLoading />
  }

  return (
        <AuthProvider>
          <Routes/>
        </AuthProvider>
    
  );
}
