const {Router} = require("express");
const router = Router();

const UserController = require('../controllers/UserController');
const ProductController = require('../controllers/ProductController');
const TrayController = require('../controllers/TrayController');
const FeedbackController = require('../controllers/FeedbackController');
const FavoriteController = require('../controllers/FavoriteController');
const validator = require("../config/validator"); // validação do usuario
const moderatorMiddleware = require("../middlewares/moderator"); // middleware do administrador
const setAuthorizationHeader = require('../middlewares/token');

//authentication uses
const AuthController = require("../controllers/AuthController");
const passport = require("passport");
router.use("/private", passport.authenticate('jwt', {session: false}));


//authentication routes
router.post("/login", AuthController.login);
router.get("/private/getDetails", AuthController.getDetails);
//
//Photo related Routes

const path = require('path');
const multer = require('multer');
const storage = require('../config/files');

const upload = multer({
    storage: storage,
    fileFilter: function(req, file, cb) {
        const ext = path.extname(file.originalname);
        if(ext !== '.png' || ext !== '.jpg' || ext !== '.jpeg') {
            return cb(new Error('Erro extensão não suportada'), false);
        }
        cb(null, true);
    },
    limits: {
        fileSize: 2048
    }
});

router.post('/User/Photos/:id', upload.single('photo'), UserController.addUserPhoto);
router.post('/Product/Photos/:id', upload.single('photo'), ProductController.addProductPhoto);
router.delete('/Photo/:id', UserController.deleteUserPhoto);
router.delete('/Photo/:id', ProductController.deleteProductPhoto);

//User Routes

router.post("/User",validator.validationUser('create'),  UserController.create);
router.get("/User", UserController.index);
router.get("/User/:id", UserController.show);
router.patch("/User/:id", moderatorMiddleware, UserController.update); //verifica se o user é um moderador
router.delete("/User/:id", UserController.destroy);

//Product Routes

router.post("/Product", ProductController.create);
router.get("/Product", ProductController.index);
router.get("/Product/:id", ProductController.show);
router.patch("/Product/:id", ProductController.update);
router.delete("/Product/:id", ProductController.destroy);

//Tray Routes

router.post("/Tray", TrayController.create);
router.get("/Tray/:userId", TrayController.show);
router.get("/Tray", TrayController.index);
router.patch("/Tray/:id", TrayController.update);
router.post("/Tray/addToTray/User/:userId/Product/:productId", TrayController.addToTray);
router.delete("/Tray/destroy/:id", TrayController.destroy);

//Feedback Routes

router.post("/Feedback", FeedbackController.create);
router.get("/Feedback", FeedbackController.index);
router.get("/Feedback/:id", FeedbackController.show);
router.delete("/Feedback/:id", FeedbackController.destroy);

//Favorite Routes

router.get("/Favorite/:userId", FavoriteController.index);
router.put("/Favorite/addToFavorite/Product/:productId/User/:userId", FavoriteController.addToFavorite);
router.put("/Favorite/removeFromFavorite/Product/:productId/User/:userId", FavoriteController.removeFromFavorite);

module.exports = router;