const User = require('../../models/User');
const faker = require('faker-br');

const seedUser = async function() {

    try {

        await User.sync({force: true});

        for(let i = 0; i < 5; i++) {

            await User.create({
                name: faker.name.firstName(),
                email: faker.internet.email(),
                cpf: faker.br.cpf(),
                address: faker.address.streetAddress(),
                userType: "Visitante",
                // picture: faker.image.image(),
                password: faker.internet.password(),
                // salt: faker.internet.password(),
            })
        }

        for(let i = 0; i < 5; i++) {

            await User.create({
                name: faker.name.firstName(),
                email: faker.internet.email(),
                cpf: faker.br.cpf(),
                address: faker.address.streetAddress(),
                userType: "Common",
                // picture: faker.image.image(),
                password: faker.internet.password(),
                // salt: faker.internet.password(),
            })
        }

        for(let i = 0; i < 5; i++) {

            await User.create({
                name: faker.name.firstName(),
                email: faker.internet.email(),
                cpf: faker.br.cpf(),
                address: faker.address.streetAddress(),
                userType: "Moderator",
                // picture: faker.image.image(),
                password: faker.internet.password(),
                // salt: faker.internet.password(),            
            })
        }

    } catch(err) {
        console.log(err);
    }

};

module.exports = seedUser;