require('../../config/dotenv')();
require('../../config/sequelize');

const seedUser = require('./UserSeeder');
const seedProduct = require('./ProductSeeder');
const seedFeedback = require('./FeedbackSeeder');
const seedTray = require('./TraySeeder');

(async () => {
  try {
    await seedUser();
    await seedProduct();
    await seedFeedback();
    // await seedTray();

  } catch(err) { console.log(err) }
})();
