const Product = require('../../models/Product');
const faker = require('faker-br');

const seedProduct = async function() {

    try {

        await Product.sync({force: true});

        for(let i = 0; i < 15; i++) {

            await Product.create({
                name: faker.commerce.productName(),
                price: faker.commerce.price(),
                // picture: faker.image.food(),
                description: faker.lorem.words(),
                seller: faker.name.firstName(),
            })
        }

    } catch(err) {
        console.log(err);
    }

};

module.exports = seedProduct;