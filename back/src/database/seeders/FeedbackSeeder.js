const Feedback = require('../../models/Feedback');
const faker = require('faker-br');

const seedFeedback = async function() {

    try {

        await Feedback.sync({force: true});

        for(let i = 0; i < 15; i++) {

            await Feedback.create({
                comment: faker.lorem.text(),
            })
        }

    } catch(err) {
        console.log(err);
    }

};

module.exports = seedFeedback;