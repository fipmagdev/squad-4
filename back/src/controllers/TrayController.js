const Tray = require('../models/Tray');
const User = require('../models/User');
const Product = require('../models/Product');

//cria bandeja vazia com metodo de pagamento definido para todos os usuarios por padrao
const create = async(req, res) => {
    try {
        const tray = await Tray.create();
        return res.status(201).json({tray});
    } catch(err) {
        res.status(500).json({error: err});
    }
};

//permite usuario alterar metodo de pagamento
const update = async(req, res) => {
    const { id } = req.params;
    try {
        const [updated] = await Tray.update(req.body, {where: {userId: id}});
        if(updated) {
            const tray = await Tray.findByPk(id);
            return res.status(200).send(tray);
        }
        throw new Error();
    } catch(err) {
        return res.status(500).json("Bandeja não encontrada");
    }
};

const addToTray = async(req, res) => {
    const { userId, productId } = req.params;
    try {
    
        let user = await User.findByPk(userId);
        const product = await Product.findByPk(productId);

        const finalPrice = async(items) => {
            let finalPrice = 0;
            for(let i = 0; i < items.length; i++) {
                const product = await Product.findOne({where: {id: items[i].productId}});
                finalPrice += product.price;
            }
            return finalPrice;
        };
        await tray.addProduct(product, {
            through: {
                message: `Produto adicionado`,
                totalPrice: finalPrice
            }
        });

        await user.addTray(product);
        await Tray.update(req.body, {where: {userId: userId, productId: productId}});

        return res.status(200).json({msg: "Produto adicionado a bandeja"});
    } catch(err) {
        return res.status(500).json({err});
    }
};

const destroy = async(req, res) => {
    const {id} = req.params;
    try {
        const deleted = await Tray.destroy({where: {userId: id}});
        if(deleted) {
            return res.status(200).json("Bandeja deletada com sucesso");
        }
        throw new Error();
    } catch(err) {
        return res.status(500).json("Bandeja não encontrada");
    }
};

const index = async(req, res) => {
    try {
        const tray = await Tray.findAll();
        return res.status(200).json({tray});
    } catch(err) {
        return res.status(500).json({err});
    }
};

const show = async(req, res) => {
    const {id} = req.params;
    try {
        const tray = await Tray.findByPk(id);
        return res.status(200).json({tray});
    } catch(err) {
        return res.status(500).json({err});
    }
};

module.exports = {
    addToTray,
    destroy,
    index,
    show,
    create,
    update
};