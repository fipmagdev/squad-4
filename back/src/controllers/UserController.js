const { Op } = require('sequelize')
const Photo = require('../models/Photo');
const User = require('../models/User');
const mailer = require('../config/mail').mailer;
const readHtml = require("../config/mail").readHTMLFile;
const path = require('path');
const hbs = require('handlebars');
const { validationResult } = require('express-validator'); // importando a biblioteca do validator
const Auth = require('..//config//Auth')

const index = async (req, res) => {
    try {
        const user = await User.findAll();
        return res.status(200).json({ user });
    } catch (err) {
        return res.status(500).json({ err });
    }
};

const show = async (req, res) => {
    const { id } = req.params;
    try {
        const user = await User.findByPk(id);
        return res.status(200).json({ user });
    } catch (err) {
        return res.status(500).json({ err });
    }
};

const create = async (req, res) => {
    try {
        validationResult(req).throw();
        const { password } = req.body;
        const HashSalt = Auth.generatePassword(password);
        const salt = HashSalt.salt;
        const hash = HashSalt.hash;
        const pathTemplate = path.resolve(__dirname, '..', '..', 'templates');
        console.log(pathTemplate);
        const newUser = {
            name: req.body.name,
            userType: req.body.userType,
            email: req.body.email,
            cpf: req.body.cpf,
            address: req.body.address,
            password: req.body.password,
            hash: hash,
            salt: salt
        };
        const user = await User.create(newUser);
        console.log(pathTemplate);
        readHtml(path.join(pathTemplate, "main.html"), (err, html) => {
            const template = handlebars.compile(html);
            const replacements = {
                username: user.name
            };
            const htmlToSend = template(replacements);
            const message = {
                from: "debandejaservicos@gmail.com",
                to: user.email,
                subject: "nodemailer funcionando corretamente",
                html: htmlToSend
            }
            mailer.sendMail(message, (err) => {
                console.log(err + "!");
            });
        });

        return res.status(201).json({ message: "Usuário cadastrado com sucesso", user: user });
    } catch (err) {
        res.status(500).json({ err });
    }
};

const update = async (req, res) => {
    const { id } = req.params;
    try {
        validationResult(req).throw();
        const [updated] = await User.update(req.body, { where: { id: id } });
        if (updated) {
            const user = await User.findByPk(id);
            return res.status(200).send(user);
        }
        throw new Error();
    } catch (err) {
        return res.status(500).json("Usuário não encontrado");
    }
};

const destroy = async (req, res) => {
    const { id } = req.params;
    try {
        const deleted = await User.destroy({ where: { id: id } });
        if (deleted) {
            return res.status(200).json("Usuário deletado com sucesso");
        }
        throw new Error();
    } catch (err) {
        return res.status(500).json("Usuário não encontrado");
    }
};

const addUserPhoto = async (req, res) => {
    try {
        const { id } = req.params;
        const user = await User.findByPk(id, { include: { model: Photo } });
        if (req.file) {
            const path = process.env.APP_URL + '/uploads/' + req.file.filename;
            console.log('path');

            const photo = await Photo.create({
                path: path,
            });
            await user.addPhoto(photo);
        }
        const userUpdated = await User.findByPk(id, { include: { model: Photo } });
        return res.status(200).json(userUpdated);
    } catch (err) {
        return res.status(500).json(err + '!');
    }
};

//remove foto
const deleteUserPhoto = async (req, res) => {
    try {
        const { id } = req.params;
        const photo = await Photo.findByPk(id);
        const pathDb = photo.path.split('/').slice(-1)[0];
        await fsPromise.unlink(path.join(__dirname, '..', '..', 'uploads', pathDb));
        await photo.destroy();
        return res.status(200).json('Foto de perfil excluída com sucesso');
    } catch (err) {
        return res.status(500).json(err + '!');
    }
};

module.exports = {
    update,
    destroy,
    create,
    show,
    index,
    addUserPhoto,
    deleteUserPhoto
};