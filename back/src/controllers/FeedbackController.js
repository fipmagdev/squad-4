const Feedback = require('../models/Feedback');

const index = async(req, res) => {
    try {
        const feedback = await Feedback.findAll();
        return res.status(200).json({feedback});
    } catch(err) {
        return res.status(500).json({err});
    }
}

const show = async(req, res) => {
    const {id} = req.params;
    try {
        const feedback = await Feedback.findByPk(id);
        return res.status(200).json({feedback});
    } catch(err) {
        return res.status(500).json({err});
    }
}

const create = async(req, res) => {
    try {
        const feedback = await Feedback.create(req.body);
        return res.status(201).json({message: "Feedback criado com sucesso", feedback: feedback});
    } catch(err) {
        res.status(500).json({error: err});
    }
}

const destroy = async(req, res) => {
    const {id} = req.params;
    try {
        const deleted = await Feedback.destroy({where: {id: id}});
        if(deleted) {
            return res.status(200).json("Feedback deletado com sucesso");
        }
        throw new Error();
    } catch(err) {
        return res.status(500).json("Feedback não encontrado");
    }
}

module.exports = {
    index,
    show,
    create,
    destroy
};