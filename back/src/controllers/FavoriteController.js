const Favorite = require('../models/Favorite');
const User = require('../models/User');
const Product = require('../models/Product');

const addToFavorite = async(req, res) => {
    const {productId, userId} = req.params;
    try {
        const product = await Product.findByPk(productId);
        let user = await User.findByPk(userId);
        await user.addProduct(product);
        await Favorite.update(req.body, {where: {productId: productId, userId: userId}, });
        return res.status(200).json({msg: "Produto favoritado com sucesso!"});
    } catch(err) {
        return res.status(500).json({err});
    }
};

const removeFromFavorite = async(req, res) => {
    const {productId, userId} = req.params;
    try {
        const product = await Product.findByPk(productId);
        let user = await User.findByPk(userId);
        await user.removeProduct(product);
        return res.status(200).json({msg: "Produto removido dos favoritos com sucesso!"});
    } catch(err) {
        return res.status(500).json({err});
    }
};

const index = async(req, res) => {
    const {userId} = req.params;
    try {
        const favorite = await Favorite.findAll({where: {userId: userId}});
        return res.status(200).json(favorite);
    } catch(err) {
        return res.status(500).json({err});
    }
};

module.exports = {
    index,
    addToFavorite,
    removeFromFavorite
};