const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const Product = sequelize.define('Product', {

    name: {
        type: DataTypes.STRING,
        allowNull: false
    },

    price: {
        type: DataTypes.DOUBLE,
        allowNull: false
    },

    description: {
        type: DataTypes.STRING,
        allowNull: false
    },

    seller: {
        type: DataTypes.STRING,
        allowNull: false
    }

}, {
    timestamps: false
});

Product.associate = function(models) {
    Product.belongsToMany(models.User, {
        through: models.Favorite,
        as: 'favorited',
    });
    Product.belongsToMany(models.User, {
        through: models.Tray,
        //foreignKey: 'productId',
        constraints: false
    });
    //Product.has(models.Photo);
}

module.exports = Product;