const DataTypes = require('sequelize');
const sequelize = require('../config/sequelize');

const Photo = sequelize.define('Photo', {
    path: {
        type: DataTypes.STRING,
        allowNull: false
    }
});

Photo.associate = function(models) {
    Photo.hasOne(models.User);
    Photo.hasOne(models.Product);
};

module.exports = Photo;