const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const Feedback = sequelize.define('Feedback', {

    comment: {
        type: DataTypes.STRING
    }
    
}, {
    timestamps: false
});

Feedback.associate = function(models) {
    Feedback.belongsTo(models.User);
    Feedback.belongsTo(models.Product);
}

module.exports = Feedback;