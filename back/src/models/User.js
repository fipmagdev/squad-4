const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const User = sequelize.define('User', {

    name: {
        type: DataTypes.STRING,
        allowNull: false
    },

    email: {
        type: DataTypes.STRING,
        allowNull: false
    },

    address: {
        type: DataTypes.STRING,
        allowNull: false
    },

    cpf: {
        type: DataTypes.STRING,
        allowNull: false
    },

    password: {
        type: DataTypes.STRING,
        allowNull: false
    },

    userType: {
        type: DataTypes.STRING,
        allowNull: false
    },

    hash: {
        type: DataTypes.STRING,
        allowNull: false
    },
    
    salt: {
        type: DataTypes.STRING,
        allowNull: false
    }

}, {
    timestamps: false
});

User.associate = function(models) {
    User.belongsToMany(models.Product, {
        through: models.Favorite,
        as: 'favoring',
    });
    User.belongsToMany(models.Product, {
        through: models.Tray,
        //foreignKey: 'userId',
        constraints: false
    });
    User.hasMany(models.Product);
    User.hasMany(models.Feedback);
    //User.hasOne(models.Photo);
};

module.exports = User;