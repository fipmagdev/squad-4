const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const Tray = sequelize.define('Tray', {

    trayId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
    },

    message: {
        type: DataTypes.STRING,
    },

    paymentMethod: {
        type: DataTypes.STRING,
    }

}, {
    timestamps: false
});

module.exports = Tray;